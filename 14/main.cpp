#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <cassert>
#include <set>
#include <stack>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
  cout << solve_first(stringstream("flqrgnkx")) << endl;  // 8108
  cout << solve_second(stringstream("flqrgnkx")) << endl;  // 1242

	auto f = stringstream("stpzcrnm");
  cout << solve_first(f) << endl;
  f.clear();
  f.seekg(0, ios::beg);
  cout << solve_second(f) << endl;

	return 0;
}

class HashGenerator
{
  vector<int> numbers;
  int skip = 0;
  int current_position = 0;

public:
  HashGenerator(int size)
    : numbers(vector<int>(size))
  {
    reset();
  }

  vector<bool> get_dense_hash(string& input)
  {
    reset();
    vector<int> hash_input;
    for (auto c : input)
    {
      hash_input.push_back(c);
    }
    int suffix[]{ 17, 31, 73, 47, 23 };
    for (auto i : suffix)
    {
      hash_input.push_back(i);
    }
    return create_dense_hash(hash_input);
  }

  vector<int> get_sparse_hash(int rounds, vector<int>& input)
  {
    reset();
    create_sparse_hash(rounds, input);
    return numbers;
  }

  void print()
  {
    for (auto num : numbers)
    {
      cout << num << " ";
    }
    cout << endl;
  }

private:

  void twist(int position, int length)
  {
    for (int i = 0; i < length / 2; i++)
    {
      int left_index = (position + i) % numbers.size();
      int right_index = (position + length - i - 1) % numbers.size();
      swap(numbers[left_index], numbers[right_index]);
    }
  }

  void round(vector<int>& lengths)
  {
    for (auto length : lengths)
    {
      twist(current_position, length);
      current_position += length;
      current_position += skip;
      skip++;
      //print();
    }
  }

  void reset()
  {
    for (int i = 0; i < numbers.size(); i++)
    {
      numbers[i] = i;
    }
    skip = 0;
    current_position = 0;
  }

  void create_sparse_hash(int rounds, vector<int>& input)
  {
    for (int i = 0; i < rounds; i++)
    {
      round(input);
    }
  }

  vector<bool> create_dense_hash(vector<int>& input)
  {
    assert(numbers.size() == 256);
    create_sparse_hash(64, input);

    vector<bool> result;
    for (int i = 0; i < 16; i++)
    {
      int accumulate = 0;
      for (int j = 0; j < 16; j++)
      {
        int index = i * 16 + j;
        accumulate ^= numbers[index];
      }
      for (int j = 0; j < 8; j++)
      {
        int offset = 7 - j;
        int mask = 1 << offset;
        result.push_back((accumulate & mask) != 0);
      }
    }

    return result;
  }
};

int solve_first(istream& input){

  string token;
  getline(input, token);
  auto generator = HashGenerator(256);

  int result = 0;
  for (int i = 0; i < 128; i++)
  {
    auto row_input = token + "-" + to_string(i);
    auto grid_line = generator.get_dense_hash(row_input);
    for (int j = 0; j < 128; j++)
    {
      if (grid_line[j])
      {
        result++;
      }
    }
  }
	return result;
}

vector<vector<bool>> get_grid(istream& input)
{
  string token;
  getline(input, token);
  auto generator = HashGenerator(256);

  vector<vector<bool>> grid{ 128 };
  for (int i = 0; i < 128; i++)
  {
    auto row_input = token + "-" + to_string(i);
    auto grid_line = generator.get_dense_hash(row_input);
    grid[i] = grid_line;
  }
  return grid;
}

typedef pair<int, int> node;
class componentcounter
{
  vector<vector<bool>>& grid;
  vector<node> deltas{ make_pair(0, 1), make_pair(0, -1), make_pair(1, 0), make_pair(-1, 0) };
  set<node> visited;
public:

  componentcounter(vector<vector<bool>>& grid)
    :grid(grid)
  {}

  int count()
  {
    int components = 0;
    for (int i = 0; i < grid.size(); i++)
    {
      for (int j = 0; j < grid[i].size(); j++)
      {
        if (!grid[i][j])
        {
          continue;
        }
        auto node = make_pair(i, j);
        if (visited.find(node) == visited.end())
        {
          components++;
          visit(node);
        }
      }
    }

    return components;
  }

private:
  void visit(node starting_node)
  {
    int count = 0;
    stack<node> next_nodes;
    next_nodes.push(starting_node);
    while (next_nodes.size() > 0)
    {
      auto current_node = next_nodes.top();
      next_nodes.pop();
      if (visited.find(current_node) != visited.end())
      {
        continue;
      }
      if (current_node.first < 0 || current_node.first >= grid.size() ||
        current_node.second < 0 || current_node.second >= grid[current_node.first].size())
      {
        continue;
      }
      if (!grid[current_node.first][current_node.second])
      {
        continue;
      }
      visited.insert(current_node);
      for (auto next : deltas)
      {
        next_nodes.push(make_pair(current_node.first + next.first, current_node.second + next.second));
      }
    }
  }
};

int solve_second(istream& input){

  auto grid = get_grid(input);
  //cout << "parsed" << endl;
  auto counter = componentcounter(grid);
  auto result = counter.count();
	return result;
}

