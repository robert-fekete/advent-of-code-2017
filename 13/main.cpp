#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("0: 3\n1: 2\n4: 4\n6: 4")) << endl;  // 24
    cout << solve_second(stringstream("0: 3\n1: 2\n4: 4\n6: 4")) << endl;  // 10

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

typedef pair<int, int> layer;

vector<layer> parse(istream& input)
{
  string line;
  vector<layer> layers;
  while (!input.eof())
  {
    getline(input, line);
    if (line == "")
    {
      continue;
    }

    string token;
    stringstream line_parser{ line };
    while (!line_parser.eof())
    {
      getline(line_parser, token, ':');
      auto first = stoi(token);
      getline(line_parser, token, ' ');
      getline(line_parser, token, ' ');
      auto second = stoi(token);
      layers.push_back(make_pair(first, second));
    }
  }

  return layers;
}

int calculate_penalty(vector<layer>& layers)
{
  int penalty = 0;
  for (auto layer : layers)
  {
    auto depth = layer.first;
    auto length = layer.second;
    if ((depth) % ((length - 1) * 2) == 0)
    {
      penalty += depth * length;
    }
  }
  return penalty;
}

bool check_if_path_clear(vector<layer>& layers, int wait_time)
{
  for (auto layer : layers)
  {
    auto depth = layer.first;
    auto length = layer.second;
    if ((wait_time + depth) % ((length - 1) * 2) == 0)
    {
      return false;
    }
  }
  return true;
}

int solve_first(istream& input){

  auto layers = parse(input);

  return calculate_penalty(layers);
}

int solve_second(istream& input){

  auto layers = parse(input);

  int wait = 0;
  while (!check_if_path_clear(layers, wait))
  {
    wait++;
  }
  return wait;
}

