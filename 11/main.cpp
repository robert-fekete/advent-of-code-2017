#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
  cout << solve_first(stringstream("se,sw,se,sw,sw")) << endl;  // 3
  cout << solve_first(stringstream("ne,ne,ne")) << endl;  // 3
  cout << solve_first(stringstream("ne,ne,sw,sw")) << endl;  // 0
  cout << solve_first(stringstream("ne,ne,s,s")) << endl;  // 2
  cout << solve_first(stringstream("ne,ne,s,s,nw")) << endl;  // 1
    cout << solve_second(stringstream("s,s,s,s,n,n,n,n")) << endl;  // 4

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

class hexvector
{
  int ne = 0;
  int n = 0;
  int nw = 0;

public:
  hexvector(int ne, int n, int nw)
    : ne(ne), n(n), nw(nw)
  {}
  
  void add(const hexvector& other)
  {
    n += other.n;
    ne += other.ne;
    nw += other.nw;
  }

  int length() const
  {
    return abs(n) + abs(ne) + abs(nw);
  }

  void reduce()
  {
    int components[3]{ ne, n, nw };
    sort(begin(components), end(components));
    int middle = components[1];
    ne -= middle;
    n -= middle;
    nw -= middle;
  }

  void print() const
  {
    cout << "ne: " << ne << " n: " << n << " nw: " << nw << endl;
  }
};

class hexvector_factory
{
  const hexvector n{ 0, -1, 0 };
  const hexvector s{ 0, 1, 0 };
  const hexvector nw{ 0, 0, 1 };
  const hexvector se{ 0, 0, -1 };
  const hexvector ne{ 1, 0, 0 };
  const hexvector sw{ -1, 0, 0 };

public:
  const hexvector& create(string& direction)
  {
    if (direction == "n")
    {
      return n;
    }
    if (direction == "s")
    {
      return s;
    }
    if (direction == "ne")
    {
      return ne;
    }
    if (direction == "sw")
    {
      return sw;
    }
    if (direction == "nw")
    {
      return nw;
    }
    if (direction == "se")
    {
      return se;
    }
  }
};

vector<hexvector> parse(istream& input)
{
  string line;
  getline(input, line);
  stringstream line_parser{ line };

  string token;
  vector<hexvector> result;
  hexvector_factory factory;
  while (!line_parser.eof())
  {
    getline(line_parser, token, ',');
    if (token != "")
    {
      result.push_back(factory.create(token));
    }
  }

  return result;
}

int solve_first(istream& input){

  hexvector position(0, 0, 0);
  auto steps = parse(input);

  for (auto step : steps)
  {
    position.add(step);
  }
  position.reduce();
  return position.length();
}

int solve_second(istream& input){

  hexvector position(0, 0, 0);
  auto steps = parse(input);
  int max_dist = 0;
  for (auto step : steps)
  {
    position.add(step);
    position.reduce();
    if (max_dist < position.length())
    {
      max_dist = position.length();
    }
  }
  return max_dist;
}

