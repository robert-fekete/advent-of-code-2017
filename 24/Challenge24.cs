﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _24
{
    internal class Challenge24 : ChallengeBase<int, int, int>
    {
        public Challenge24()
        {
            var input = File.ReadAllText(@".\input.txt");
            var testInput = @"0/2
                        2/2
                        2/3
                        3/4
                        3/5
                        0/1
                        10/1
                        9/10";

            RegisterFirstTestCase(testInput, 0, 31);
            RegisterFirstRun(input, 0);
            RegisterSecondTestCase(testInput, 0, 19);
            RegisterSecondRun(input, 0);
        }

        protected override int SolveFirst(string input, int param)
        {
            var components = ParseInput(input);
            var traversal = new Traversal();

            var value = traversal.GetMaxStrengthSubTreeSum(components);

            return value;
        }

        protected override int SolveSecond(string input, int param)
        {
            var components = ParseInput(input);
            var traversal = new Traversal();

            var value = traversal.GetMaxLengthSubTreeSum(components);

            return value;
        }

        private IEnumerable<BridgeComponent> ParseInput(string input)
        {
            var nodes = new List<BridgeComponent>();
            var lines = input.Split(Environment.NewLine).Select(l => l.Trim());

            foreach (var line in lines)
            {
                var parts = line.Split('/');
                var first = int.Parse(parts[0]);
                var second = int.Parse(parts[1]);
                nodes.Add(new BridgeComponent(first, second));
            }

            return nodes;
        }
    }
}
