﻿using _23.Assembly;

namespace _23.Instructions
{
    internal class SetInstruction : BinaryInstruction
    {
        private readonly string registerName;

        public SetInstruction(string registerName, int constant) : base(registerName, constant)
        {
            this.registerName = registerName;
        }

        public SetInstruction(string registerName1, string registerName2) : base(registerName1, registerName2)
        {
            registerName = registerName1;
        }

        protected override void Executelongernal(Registry registry, ref int instructionPolonger, ref long solutionCounter)
        {
            var value = second.GetValue(registry);
            registry.Set(registerName, value);
        }
    }
}
