﻿using _23.Assembly;

namespace _23.Instructions
{
    internal class JnzInstruction : BinaryInstruction
    {
        public JnzInstruction(int constant1, int constant2) : base(constant1, constant2)
        {
        }

        public JnzInstruction(string registerName, int constant) : base(registerName, constant)
        {
        }

        public JnzInstruction(string registerName1, string registerName2) : base(registerName1, registerName2)
        {
        }

        public JnzInstruction(int constant, string registerName) : base(constant, registerName)
        {
        }

        protected override void Executelongernal(Registry registry, ref int instructionPointer, ref long solutionCounter)
        {
            var value = first.GetValue(registry);
            var offset = second.GetValue(registry);

            if (value != 0)
            {
                instructionPointer += (offset - 1);  // +1 is compensating for the usual increment 
            }
        }
    }
}
