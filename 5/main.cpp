#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("0\n3\n0\n1\n-3\n")) << endl;  // 5
    cout << solve_second(stringstream("0\n3\n0\n1\n-3\n")) << endl;  // 10

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

int solve_jumps(istream& input, function<void(int&)> modificator)
{
  int steps = 0;
  vector<int> jumps;
  string token;
  while (!input.eof())
  {
    getline(input, token);
    if (token != "")
    {
      jumps.push_back(stoi(token));
    }
  }

  int position = 0;
  while (position >= 0 && position < jumps.size())
  {
    int offset = jumps[position];
    modificator(jumps[position]);
    position += offset;
    steps++;
  }

  return steps;
}

int solve_first(istream& input){

  return solve_jumps(input, [](int& offset) { offset++; });
}

int solve_second(istream& input){
  return solve_jumps(input, [](int& offset) 
  {
    if (offset >= 3)
    {
      offset--;
    }
    else
    {
      offset++;
    }
  });
}

