#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
  for (int i = 2; i < 26; i++)
  {
    //cout << solve_first(stringstream(to_string(i))) << endl;
  }

  cout << solve_first(stringstream("1")) << endl;  // 0
  cout << solve_first(stringstream("9")) << endl;  // 2
  cout << solve_first(stringstream("12")) << endl;  // 3
  cout << solve_first(stringstream("23")) << endl;  // 2
  cout << solve_first(stringstream("1024")) << endl;  // 31
  cout << solve_second(stringstream("1")) << endl;  // 1
  cout << solve_second(stringstream("10")) << endl;  // 11
  cout << solve_second(stringstream("20")) << endl;  // 23
  cout << solve_second(stringstream("40")) << endl;  // 54
  cout << solve_second(stringstream("50")) << endl;  // 54

	auto f = stringstream("347991");
	
	cout << solve_first(f) << endl;
	f.clear();
	f.seekg(0, ios::beg);
	cout << solve_second(f) << endl;

	return 0;
}

pair<int, int> get_coordinates(int position)
{
  if (position == 1)
  {
    return make_pair(0, 0);
  }

  auto lower_square = (int)floor(sqrt(position));
  auto inner_circle_radius = lower_square % 2 == 1 ? lower_square : lower_square - 1;
  inner_circle_radius = inner_circle_radius * inner_circle_radius == position ? inner_circle_radius - 2 : inner_circle_radius;
  auto size_of_side = inner_circle_radius + 1;
  auto outer_circle_size = position - inner_circle_radius * inner_circle_radius - 1;

  auto index_of_side = outer_circle_size / size_of_side;
  auto size_of_last_side = outer_circle_size % size_of_side;

  auto radius = (inner_circle_radius + 1) / 2;
  int x, y;
  switch (index_of_side)
  {
  case 0:
    x = radius;
    y = -radius + 1 + size_of_last_side;
    break;
  case 1:
    y = radius;
    x = radius - 1 - size_of_last_side;
    break;
  case 2:
    x = -radius;
    y = radius - 1 - size_of_last_side;
    break;
  case 3:
    y = -radius;
    x = -radius + 1 + size_of_last_side;
    break;
  }

  //cout << position << "-> " << x << " : " << y << endl;

  return make_pair(x, y);
}

int solve_first(istream& input)
{
  string token;
  input >> token;
  int position = stoi(token);

  auto point = get_coordinates(position);

  return abs(point.first) + abs(point.second);
}

bool are_neighbours(pair<int, int> a, pair<int, int> b)
{
  return a.first - 1 <= b.first && a.first + 1 >= b.first &&
    a.second - 1 <= b.second && a.second + 1 >= b.second;
}

int solve_second(istream& input)
{
  string token;
  input >> token;
  int offset = stoi(token);

  vector<int> values;
  values.push_back(-100000);
  int current_value = 1;
  values.push_back(current_value);
  int index = 2;
  while (current_value <= offset)
  {
    int next_value = 0;
    auto node = get_coordinates(index);
    for (int i = 1; i < index; i++)
    {
      auto other_node = get_coordinates(i);
      if (are_neighbours(node, other_node))
      {
        next_value += values[i];
      }
    }
    values.push_back(next_value);
    current_value = next_value;
    index++;
  }  

  return current_value;
}
