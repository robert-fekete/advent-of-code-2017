﻿using System.Collections.Generic;
using System.Linq;

namespace _23.Assembly
{
    internal class Registry
    {
        private readonly IDictionary<string, int> values;

        public Registry(IDictionary<string, int> initialValues)
        {
            values = initialValues;
        }

        public int Get(string name)
        {
            return values[name];
        }

        public void Set(string name, int value)
        {
            values[name] = value;
        }

        public void Print()
        {
            foreach (var name in values.Keys.OrderBy(a => a))
            {
                System.Console.WriteLine($"{name}: {values[name]}");
            }
        }
    }
}
