#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <stack>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
  cout << solve_first(stringstream("{{<a!>},{<a!>},{<a!>},{<ab>}}")) << endl;  // 3
  cout << solve_first(stringstream("{{{},{},{{}}}}")) << endl;  // 16
  cout << solve_second(stringstream("<{o\"i!a,<{i<a>")) << endl;  // 10
  cout << solve_second(stringstream("<!!!>>")) << endl;  // 0

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

class stacksfm
{
  stack<function<void(char)>> states;
  
public:
  
  void push_state(function<void(char)> state)
  {
    states.push(state);
  }

  void pop_state()
  {
    states.pop();
  }

  function<void(char)> get_current_state()
  {
    return states.top();
  }
};

class parser
{
  stacksfm state_machine;
  function<void(char)> in_group_action = [](char) {};
  function<void(char)> in_garbage_action = [](char) {};
  function<void(char)> in_cancelled_action = [](char) {};

public:
  
  parser()
  {
    auto fp = std::bind(&parser::in_group, this, std::placeholders::_1);
    state_machine.push_state(fp);
  }

  void update(char c)
  {
    auto current_state = state_machine.get_current_state();
    current_state(c);
  }

  void set_group_action(function<void(char)> action)
  {
    in_group_action = action;
  }

  void set_garbage_action(function<void(char)> action)
  {
    in_garbage_action = action;
  }

  void set_cancelled_action(function<void(char)> action)
  {
    in_cancelled_action = action;
  }

private:

  void in_group(char c)
  {
    if (c == '{')
    {
      auto fp = bind(&parser::in_group, this, std::placeholders::_1);
      state_machine.push_state(fp);
    }

    if (c == '}')
    {
      state_machine.pop_state();
    }

    if (c == '<')
    {
      auto fp = bind(&parser::in_garbage, this, std::placeholders::_1);
      state_machine.push_state(fp);
    }

    if (c == '!')
    {
      auto fp = bind(&parser::ignore_char, this, std::placeholders::_1);
      state_machine.push_state(fp);
    }

    in_group_action(c);
  }

  void in_garbage(char c)
  {
    if (c == '!')
    {
      auto fp = bind(&parser::ignore_char, this, std::placeholders::_1);
      state_machine.push_state(fp);
    }
    if (c == '>')
    {
      state_machine.pop_state();
    }

    in_garbage_action(c);
  }

  void ignore_char(char c)
  {
    state_machine.pop_state();

    in_cancelled_action(c);
  }
};

void parse_input(istream& input, parser& parser)
{
  char c;
  while (!input.eof())
  {
    c = input.get();
    parser.update(c);
  }
}

int solve_first(istream& input){

  int depth = 0;
  int score = 0;
  parser parser;
  parser.set_group_action([&depth, &score](char c)
  {
    if (c == '{')
    {
      depth++;
      score += depth;
    }
    if (c == '}')
    {
      depth--;
    }
  });

  parse_input(input, parser);

	return score;
}

int solve_second(istream& input){

  int garbage = 0;
  parser parser;
  parser.set_garbage_action([&garbage](char c)
  {
    if (c != '!' && c != '>')
    {
      garbage++;
    }
  });

  parse_input(input, parser);

  return garbage;
}

