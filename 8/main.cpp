#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
  cout << solve_first(stringstream("b inc 5 if a > 1\na inc 1 if b < 5\nc dec -10 if a >= 1\nc inc -20 if c == 10")) << endl;  // 1
  cout << solve_second(stringstream("b inc 5 if a > 1\na inc 1 if b < 5\nc dec -10 if a >= 1\nc inc -20 if c == 10")) << endl;  // 10

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

class registers
{
  map<string, int> known_registers;
  int overall_max;

public:
  int get_value(string name)
  {
    if (known_registers.find(name) == known_registers.end())
    {
      known_registers[name] = 0;
    }
    return known_registers[name];
  }

  void set_value(string name, int value)
  {
    known_registers[name] = value;

    int current_max = max_value();
    if (current_max > overall_max)
    {
      overall_max = current_max;
    }
  }

  int max_value()
  {
    int max_value = known_registers.begin()->second;
    for (auto value : known_registers)
    {
      if (value.second > max_value)
      {
        max_value = value.second;
      }
    }

    return max_value;
  }

  int overall_max_value()
  {
    return overall_max;
  }
};

typedef function<void(registers&)> instruction;

map<string, function<int(int, int)>> operators = {
  { "inc", [](int num, int offset) { return num + offset; } },
  { "dec", [](int num, int offset) { return num - offset; } }
};

map<string, function<bool(int, int)>> predicates = {
  { "<", [](int num, int offset) { return num < offset; } },
  { "<=", [](int num, int offset) { return num <= offset; } },
  { ">", [](int num, int offset) { return num > offset; } },
  { ">=", [](int num, int offset) { return num >= offset; } },
  { "==", [](int num, int offset) { return num == offset; } },
  { "!=", [](int num, int offset) { return num != offset; } }
};

instruction parse_instruction(string& line)
{
  auto line_parser = stringstream{ line };
  string token;
  getline(line_parser, token, ' ');
  auto target_register = token;
  getline(line_parser, token, ' ');
  auto operator_name = token;
  getline(line_parser, token, ' ');
  auto offset = stoi(token);

  getline(line_parser, token, ' ');

  getline(line_parser, token, ' ');
  auto condition_name = token;
  getline(line_parser, token, ' ');
  auto condition_operator_name = token;
  getline(line_parser, token, ' ');
  auto condition_offset = stoi(token);

  return [target_register, operator_name, offset, condition_name, condition_operator_name, condition_offset](registers& registers)
  {
    if (predicates[condition_operator_name](registers.get_value(condition_name), condition_offset))
    {
      registers.set_value(target_register, operators[operator_name](registers.get_value(target_register), offset));
    }
  };
}

vector<instruction> parse_instructions(istream& input)
{
  string line;
  vector<instruction> instructions;
  while (!input.eof())
  {
    getline(input, line);
    if (line != "")
    {
      instructions.push_back(parse_instruction(line));
    }
  }

  return instructions;
}

int solve_first(istream& input){

  registers regs;
  auto instructions = parse_instructions(input);
  for (auto i : instructions)
  {
    i(regs);
  }

	return regs.max_value();
}

int solve_second(istream& input){
  registers regs;
  auto instructions = parse_instructions(input);
  for (auto i : instructions)
  {
    i(regs);
  }
  return regs.overall_max_value();
}

