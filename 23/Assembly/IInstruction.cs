﻿namespace _23.Assembly
{
    internal interface IInstruction
    {
        void Execute(Registry registry, ref int instructionPointer, ref long solutionCounter);
    }
}