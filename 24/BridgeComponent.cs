﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace _24
{
    [DebuggerDisplay("{first}/{second}")]
    internal class BridgeComponent
    {
        private readonly int first;
        private readonly int second;

        private readonly List<int> freePorts = new List<int>();

        public BridgeComponent(int first, int second)
        {
            this.first = first;
            this.second = second;

            ReleaseAll();
        }

        public int Strength => first + second;
        public int FreePort => freePorts.First();

        internal bool CanContinueWith(BridgeComponent next)
        {
            return next.HasPort(FreePort);
        }

        private bool HasPort(int port)
        {
            return freePorts.Contains(port);
        }

        internal void Take(int port)
        {
            if (!freePorts.Contains(port))
            {
                throw new InvalidOperationException($"Trying to take port that doesn't exist or not free: {port}");
            }
            freePorts.Remove(port);
        }

        internal void ReleaseAll()
        {
            freePorts.Clear();
            freePorts.Add(first);
            freePorts.Add(second);
        }
    }
}
