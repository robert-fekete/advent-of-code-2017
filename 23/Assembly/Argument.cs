﻿namespace _23.Assembly
{
    internal class Argument
    {
        private readonly string registerName;
        private readonly int constant;

        private readonly bool isConstant;

        public Argument(string registerName)
        {
            this.registerName = registerName;
            isConstant = false;
        }

        public Argument(int constant)
        {
            this.constant = constant;
            isConstant = true;
        }

        public int GetValue(Registry registry)
        {
            return isConstant ? constant : registry.Get(registerName);
        }
    }
}
