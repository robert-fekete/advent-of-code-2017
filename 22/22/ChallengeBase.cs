﻿using System;
using System.Collections.Generic;

namespace _22
{
    internal abstract class ChallengeBase<TParam, TParam2, TResult> where TResult : IComparable
    {
        private readonly List<(string, TParam, TResult)> firstTestCases = new List<(string, TParam, TResult)>();
        private readonly List<(string, TParam2, TResult)> secondTestCases = new List<(string, TParam2, TResult)>();
        private (string Input, TParam Param) firstRunParams;
        private (string Input, TParam2 Param) secondRunParams;

        public void Run()
        {
            Run(SolveFirst, firstTestCases, firstRunParams.Input, firstRunParams.Param);
            Console.WriteLine();
            Run(SolveSecond, secondTestCases, secondRunParams.Input, secondRunParams.Param);
        }

        protected void RegisterFirstRun(string input, TParam param)
        {
            firstRunParams = (input, param);
        }

        protected void RegisterSecondRun(string input, TParam2 param)
        {
            secondRunParams = (input, param);
        }

        protected void RegisterFirstTestCase(string input, TParam param, TResult expected)
        {
            firstTestCases.Add((input, param, expected));
        }

        protected void RegisterSecondTestCase(string input, TParam2 param, TResult expected)
        {
            secondTestCases.Add((input, param, expected));
        }

        private void Run<TParamType>(Func<string, TParamType, TResult> action,
                                     IEnumerable<(string, TParamType, TResult)> testCases,
                                     string input,
                                     TParamType param)
        {
            bool failed = false;
            foreach ((var testInput, var testParam, var expected) in testCases)
            {
                var actual = action(testInput, testParam);
                if (expected.CompareTo(actual) != 0)
                {
                    failed = true;
                    Console.WriteLine($"Failed. Actual: {actual}, Expected: {expected}");
                }
                else
                {
                    Console.WriteLine($"Test passed: {actual}");
                }
            }

            if (!failed)
            {
                var solution = action(input, param);
                Console.WriteLine($"Solution: {solution}");
            }
        }

        protected abstract TResult SolveFirst(string input, TParam param);

        protected abstract TResult SolveSecond(string input, TParam2 param);
    }
}
