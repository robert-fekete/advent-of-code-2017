﻿using System;

namespace _23
{
    class Program
    {
        static void Main(string[] args)
        {
            var challenge = new Challenge23();
            challenge.Run();
        }
    }
}
