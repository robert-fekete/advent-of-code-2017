#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <bitset>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("Generator A starts with 65\nGenerator B starts with 8921")) << endl;  // 588
    cout << solve_second(stringstream("Generator A starts with 65\nGenerator B starts with 8921")) << endl;  // 309

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

int parse_line(string line)
{
  stringstream line_parser{ line };
  string token;
  getline(line_parser, token, ' ');
  getline(line_parser, token, ' ');
  getline(line_parser, token, ' ');
  getline(line_parser, token, ' ');
  getline(line_parser, token, ' ');

  return stoi(token);
}

pair<int, int> parse(istream& input)
{
  string line;
  getline(input, line);
  int a = parse_line(line);
  getline(input, line);
  int b = parse_line(line);

  return make_pair(a, b);
}


class generator
{
  const unsigned short multiplier;
  const unsigned int modulo_factor = 2147483647;

protected:
  unsigned long long value;

public:
  generator(int seed, unsigned short multiplier)
    : value(seed), multiplier(multiplier)
  { }

  unsigned short get_next_value(unsigned short mask)
  {
    generate_next_value();
    return value & mask;
  }

protected:

  virtual void generate_next_value()
  {
    value = (value * multiplier) % modulo_factor;
  }
};

int get_number_of_matches(generator& a, generator& b, int loops)
{
  const auto mask = (1 << 16) - 1;

  int counter = 0;
  for (int i = 0; i < loops; i++)
  {
    auto a_comp = a.get_next_value(mask);
    auto b_comp = b.get_next_value(mask);
    if (a_comp == b_comp)
    {
      counter++;
    }
  }

  return counter;
}

int solve_first(istream& input){

  auto starting_numbers = parse(input);

  auto a = generator{ starting_numbers.first, 16807 };
  auto b = generator{ starting_numbers.second, 48271 };
  
	return get_number_of_matches(a, b, 40000001);
}

class criteriagenerator
  : public generator
{
  int multiples_of;
public:
  criteriagenerator(int seed, unsigned short multiplier, int multiples_of)
    : generator(seed, multiplier),
    multiples_of(multiples_of)
  { }

private:
  void generate_next_value()
  {
    do
    {
      generator::generate_next_value();
    }
    while (value % multiples_of != 0);
  }
};

int solve_second(istream& input){

  auto starting_numbers = parse(input);

  auto a = criteriagenerator{ starting_numbers.first, 16807, 4 };
  auto b = criteriagenerator{ starting_numbers.second, 48271, 8 };

  return get_number_of_matches(a, b, 5000001);
}

