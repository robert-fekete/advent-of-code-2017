﻿using System;

namespace _25
{
    internal class State
    {
        private readonly Operation ifZero;
        private readonly Operation ifOne;

        public State(Operation ifZero, Operation ifOne)
        {
            this.ifZero = ifZero;
            this.ifOne = ifOne;
        }

        internal string Execute(Cursor cursor)
        {
            if (cursor.Value)
            {
                return ifOne.Execute(cursor);
            }
            else
            {
                return ifZero.Execute(cursor);
            }
        }
    }
}