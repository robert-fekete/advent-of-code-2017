﻿using _23.Assembly;
using _23.Instructions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _23
{
    internal class Challenge23 : ChallengeBase<int, int, long>
    {
        public Challenge23()
        {
            var input = File.ReadAllText(@".\input.txt");
            RegisterFirstRun(input, 0);
            RegisterSecondRun(input, 0);
        }

        private Dictionary<string, int> EmptyRegistry => new Dictionary<string, int>
        {
            { "a", 0 },
            { "b", 0 },
            { "c", 0 },
            { "d", 0 },
            { "e", 0 },
            { "f", 0 },
            { "g", 0 },
            { "h", 0 },
        };

        protected override long SolveFirst(string input, int param)
        {
            var factory = new ProcessorFactory(Parse);
            
            var processor = factory.Create(input, EmptyRegistry, false);
            return processor.Execute();
        }

        // God damnit I hate this assignment, there is a reason I am not an assembly programmer.....
        protected override long SolveSecond(string input, int param)
        {
            var from = 105700;
            var to = 122700;

            var count = 0;
            var primes = GeneratePrimes(to + 1);
            for(int i = from; i <= to; i+=17)
            {
                if (!primes.Contains(i))
                {
                    count++;
                }
            }

            return count;
        }

        private HashSet<int> GeneratePrimes(int to)
        {
            var numbers = new List<bool>(Enumerable.Repeat(true, to));

            numbers[0] = numbers[1] = false;
            for(int i = 2; i < to; i++)
            {
                for(int iter = i + i; iter < to; iter += i)
                {
                    numbers[iter] = false;
                }
            }

            var primes = new List<int>();
            for (int i = 0; i < to; i++)
            {
                if (numbers[i])
                {
                    primes.Add(i);
                }
            }

            return new HashSet<int>(primes);
        }

        private IInstruction Parse(string line)
        {
            var parts = line.Split(' ');
            switch (parts[0])
            {
                case "set":
                    {
                        if (int.TryParse(parts[2], out int second))
                        {
                            return new SetInstruction(parts[1], second);
                        }
                        else
                        {
                            return new SetInstruction(parts[1], parts[2]);
                        }
                    }
                case "sub":
                    {
                        if (int.TryParse(parts[2], out int second))
                        {
                            return new SubInstruction(parts[1], second);
                        }
                        else
                        {
                            return new SubInstruction(parts[1], parts[2]);
                        }
                    }
                case "mul":
                    {
                        if (int.TryParse(parts[2], out int second))
                        {
                            return new MulInstruction(parts[1], second);
                        }
                        else
                        {
                            return new MulInstruction(parts[1], parts[2]);
                        }
                    }
                case "jnz":
                    {
                        var isFirstInt = int.TryParse(parts[1], out int first);
                        var isSecondInt = int.TryParse(parts[2], out int second);
                        if (isFirstInt && isSecondInt)
                        {
                            return new JnzInstruction(first, second);
                        }
                        else if (isFirstInt)
                        {
                            return new JnzInstruction(first, parts[2]);
                        }
                        else if (isSecondInt)
                        {
                            return new JnzInstruction(parts[1], second);
                        }
                        else
                        {
                            return new JnzInstruction(parts[1], parts[2]);
                        }
                    }
                default:
                    throw new InvalidOperationException($"Invalid instruction: {parts[0]}");
            }
        }
    }
}
