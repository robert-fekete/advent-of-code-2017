﻿using System;
using System.IO;

namespace _22
{
    internal class Challenge22 : ChallengeBase<long, long, long>
    {
        public Challenge22()
        {
            var input = File.ReadAllText(@".\input.txt");
            var testInput = @"..#
                         #.. 
                         ...";
            RegisterFirstTestCase(testInput, 7, 5);
            RegisterFirstTestCase(testInput, 70, 41);

            RegisterFirstRun(input, 10000);

            RegisterSecondTestCase(testInput, 100, 26);
            RegisterSecondTestCase(testInput, 10000000, 2511944);

            RegisterSecondRun(input, 10000000);
        }

        protected override long SolveFirst(string input, long numberOfBursts)
        {
            var map = GridMap.FromInput(input);
            var worm = Worm.FromInput(input);
            //map.Print(-3, -3, 9, 9);

            long numberOfInfestation = 0;
            for (long i = 0; i < numberOfBursts; i++)
            {
                var isInfested = map.IsInfested(worm.Position);
                if (isInfested)
                {
                    worm.TurnRight();
                    map.Clean(worm.Position);
                }
                else
                {
                    worm.TurnLeft();
                    map.Infest(worm.Position);
                    numberOfInfestation++;
                }
                worm.Move();

                //map.Print(-3, -3, 9, 9);
            }

            return numberOfInfestation;
        }

        protected override long SolveSecond(string input, long numberOfBursts)
        {
            var map = GridMap.FromInput(input);
            var worm = Worm.FromInput(input);
            //map.Print(-3, -3, 9, 9);

            long numberOfInfestation = 0;
            for (long i = 0; i < numberOfBursts; i++)
            {
                if (map.IsInfested(worm.Position))
                {
                    worm.TurnRight();
                    map.Flag(worm.Position);
                }
                else if (map.IsFlagged(worm.Position))
                {
                    worm.Reverse();
                    map.Clean(worm.Position);
                }
                else if(map.IsWeakend(worm.Position))
                {
                    map.Infest(worm.Position);
                    numberOfInfestation++;
                }
                else
                {
                    worm.TurnLeft();
                    map.Weaken(worm.Position);
                }
                worm.Move();

                //map.Print(-3, -3, 9, 9);
            }

            return numberOfInfestation;
        }
    }
}
