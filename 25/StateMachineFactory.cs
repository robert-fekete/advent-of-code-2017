﻿using System;
using System.Collections.Generic;

namespace _25
{
    internal class StateMachineFactory
    {
        public static StateMachine FromInput(string[] lines)
        {
            var startingState = lines[0].Split(' ')[3].Trim('.');

            var states = new Dictionary<string, State>();
            for(int i = 2; i < lines.Length; i += 9)
            {
                (var name, var state) = ParseState(lines, i);
                states[name] = state;
            }

            return new StateMachine(states, startingState, new Cursor());
        }

        private static (string, State) ParseState(string[] lines, int index)
        {
            // Name
            var name = lines[index].Split(' ')[2].Trim(':');

            index++;
            var zeroOperation = ParseOperation(lines, ref index);
            index++;
            var oneOperation = ParseOperation(lines, ref index);

            return (name, new State(zeroOperation, oneOperation));
        }

        private static Operation ParseOperation(string[] lines, ref int index)
        {
            index++;
            var writeValue = int.Parse(lines[index].Trim('.', ' ', '-').Split(' ')[3]) == 1;

            index++;
            var offsetParts = lines[index].Trim('.', ' ', '-').Split(' ');
            var moveOffsetValue = ParseNumberString(offsetParts[1]);
            var moveOffsetSignal = offsetParts[5] == "right" ? 1 : -1;
            var moveOffset = moveOffsetSignal * moveOffsetValue;

            index++;
            var nextState = lines[index].Trim('.', ' ', '-').Split(' ')[3];

            return new Operation(writeValue, moveOffset, nextState);
        }

        private static int ParseNumberString(string token)
        {
            switch (token)
            {
                case "one":
                    return 1;
                case "two":
                    return 2;
                default:
                    throw new InvalidOperationException($"Invalid number token {token}");
            }
        }
    }
}
