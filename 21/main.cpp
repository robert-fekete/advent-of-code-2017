#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <set>

using namespace std;

int solve_first(istream&, int);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("../.# => ##./#../...\n.#./..#/### => #..#/..../..../#..#"), 2) << endl;  // 12

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f, 5) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_first(f, 18) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

typedef vector<char> sequence;
typedef vector<sequence> sub_grid;

class rule
{
  set<sub_grid> starting_patterns;
  sub_grid to;
public:
  rule(sub_grid from, sub_grid to)
    : to(to)
  {
	  for (int i = 0; i < 4; i++) 
		{
			starting_patterns.insert(from);
			from = rotate(from);
		}

		from = flip(from);

		for (int i = 0; i < 4; i++)
		{
			starting_patterns.insert(from);
			from = rotate(from);
		}
  }

  bool is_match(const sub_grid& part)
  {
    if (starting_patterns.begin()->size() != part.size())
    {
      return false;
    }

		for (const auto& from : starting_patterns)
		{
			if (is_match_from(from, part))
			{
				return true;
			}
		}

    return false;
  }

  sub_grid* get_enhanced()
  {
    return &to;
  }

private: 

  sub_grid rotate(sub_grid& part)
  {
    auto new_grid = sub_grid(part.size(), sequence(part[0].size()));


    for (int i = 0; i < part.size(); i++)
    {
      for (int j = 0; j < part[0].size(); j++)
      {
        new_grid[j][part.size() - i - 1] = part[i][j];
      }
    }

    return new_grid;
  }

  sub_grid flip(sub_grid& part)
  {
		auto new_grid = sub_grid(part.size(), sequence(part[0].size()));

    for (int i = 0; i < part.size(); i++)
    {
      for (int j = 0; j < part[0].size(); j++)
      {
        new_grid[part.size() - i - 1][j] = part[i][j];
      }
    }

return new_grid;
	}

	bool is_match_from(const sub_grid& from, const sub_grid& part)
	{
		for (int i = 0; i < part.size(); i++)
		{
			for (int j = 0; j < part[0].size(); j++)
			{
				if (from[i][j] != part[i][j])
				{
					return false;
				}
			}
		}

		return true;
	}
};

class ruleset
{
	vector<rule> rules;
public:

	void add(rule rule)
	{
		rules.push_back(rule);
	}

	sub_grid* get_enhanced(const sub_grid& part)
	{
		for (auto& rule : rules)
		{
			if (rule.is_match(part))
			{
				return rule.get_enhanced();
			}
		}
		throw logic_error("No matching rule");
	}
};


class grid
{
	vector<sub_grid*> parts;
	size_t size = 0;
	size_t current_size = 0;
	int dimension = 0;
	int resolution = 0;

public:

	grid(size_t dimension, size_t part_size)
		:size(dimension * dimension),
		dimension(dimension),
		parts(vector<sub_grid*>(dimension * dimension))
	{
		resolution = dimension * part_size;
	}

	void add(sub_grid* part)
	{
		parts[current_size++] = part;
	}

	vector<sub_grid> devide()
	{
		if (resolution % 2 == 0)
		{
			return devide(2);
		}
		else
		{
			return devide(3);
		}
	}

	int count(const function<bool(char)>& predicate)
	{
		int count = 0;
		for (auto part : parts)
		{
			for (auto& seq : *part)
			{
				for (auto& pixel : seq)
				{
					if (predicate(pixel))
					{
						count++;
					}
				}
			}
		}

		return count;
	}

	size_t get_next_dimension()
	{
		if (resolution % 2 == 0)
		{
				return resolution / 2;
		}
		else
		{
			return resolution / 3;
		}
	}

  void print()
  {
    for (int i = 0; i < dimension; i++)
    {
      for (int x = 0; x < parts[0]->size(); x++)
      {
        for (int j = 0; j < dimension; j++)
        {
          auto& part = *parts[i * dimension + j];
          for (int y = 0; y < parts[0][0].size(); y++)
          {
            cout << part[x][y];
          }
        }
        cout << endl;
      }
    }
    cout << endl;
  }

private:

  vector<sub_grid> devide(int devide_by)
  {
    auto new_dimension = resolution / devide_by;
    auto result = vector<sub_grid>(new_dimension * new_dimension, vector<sequence>(devide_by, sequence(devide_by)));

    for (int row = 0; row < resolution; row++)
    {
      for (int col = 0; col < resolution; col++)
      {
        int from_row = row / parts[0]->size();
        int from_col = col / parts[0]->size();
        auto& from_part = *parts[from_row * dimension + from_col];

        int to_row = row / devide_by;
        int to_col = col / devide_by;
        auto& to_part = result[to_row * new_dimension + to_col];

        int from_part_row = row % parts[0]->size();
        int from_part_col = col % parts[0]->size();

        int to_part_row = row % devide_by;
        int to_part_col = col % devide_by;
        
        to_part[to_part_row][to_part_col] = from_part[from_part_row][from_part_col];
      }
    }

    return move(result);
  }
};

class art
{
  ruleset& rules;
  grid image;

public:
  art(ruleset& rules, grid initial_grid)
    : rules(rules),
    image(initial_grid)
  {}

  void enhance()
  {
    auto parts = image.devide();

    auto new_image = grid(image.get_next_dimension(), parts[0].size() + 1);
    for (const auto& part:parts)
    {
      auto enhanced_part = rules.get_enhanced(part);
      new_image.add(enhanced_part);
    }

    //new_image.print();
    image = new_image;
  }

  int get_on_pixels()
  {
    return image.count([](char c) { return c == '#'; });
  }
};

sub_grid parse_pattern(string& pattern_string)
{
  sub_grid result;
  stringstream pattern_parser{ pattern_string };
  string token;
  while (!pattern_parser.eof())
  {
    getline(pattern_parser, token, '/');
    if (token != "")
    {
      result.push_back(sequence{ begin(token), end(token) });
    }
  }

  return result;
}

rule parse_line(string& line)
{
  string token;
  stringstream line_parser{ line };
  getline(line_parser, token, ' ');
  auto from = parse_pattern(token);
  getline(line_parser, token, ' ');
  getline(line_parser, token, ' ');
  auto to = parse_pattern(token);

  return rule(from, to);
}

ruleset parse(istream& input)
{
  string line;
  ruleset rules;
  while (!input.eof())
  {
    getline(input, line);
    if (line != "")
    {
      auto rule = parse_line(line);
      rules.add(rule);
    }
  }

  return rules;
}


int solve_first(istream& input, int iterations){

  auto rules = parse(input);
  auto initial_grid = grid(1, 3);
	auto starting_grid = sub_grid{ { '.', '#', '.' },{ '.', '.', '#' },{ '#', '#', '#' } };
  initial_grid.add(&starting_grid);
  auto image = art{ rules, initial_grid };

	for (int i = 0; i < iterations; i++)
	{
		image.enhance();
	}

  auto on_pixels = image.get_on_pixels();
	return on_pixels;
}

