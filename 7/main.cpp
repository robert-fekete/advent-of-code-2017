#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <set>
#include <cassert>

using namespace std;

string solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
  cout << solve_first(stringstream("pbga (66)\nxhth (57)\nebii (61)\nhavc (66)\nktlj (57)\nfwft (72) -> ktlj, cntj, xhth\nqoyq (66)\npadx (45) -> pbga, havc, qoyq\ntknk (41) -> ugml, padx, fwft\njptl (61)\nugml (68) -> gyxo, ebii, jptl\ngyxo (61)\ncntj (57)")) << endl;  // tknk
  cout << solve_second(stringstream("pbga (66)\nxhth (57)\nebii (61)\nhavc (66)\nktlj (57)\nfwft (72) -> ktlj, cntj, xhth\nqoyq (66)\npadx (45) -> pbga, havc, qoyq\ntknk (41) -> ugml, padx, fwft\njptl (61)\nugml (68) -> gyxo, ebii, jptl\ngyxo (61)\ncntj (57)")) << endl;  // 60
  cout << solve_second(stringstream("root (50) -> a, b, c\na (10) -> aa, ab, ac\naa (6)\nab (6)\nac (6)\nb (10) -> ba, bb, bc\nba (6)\nbb (6)\nbc (6)\nc (10) -> ca, cb\nca (6) -> caa, cab, cac\ncaa (1)\ncab (1)\ncac (1)\ncb (5) -> cba, cbb, cbc\ncba (2)\ncbb (2)\ncbc (2)")); // 3

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

typedef pair<string, int> node;

pair<node, vector<string>> parse(string& line)
{
  string token;
  auto line_parser = stringstream{ line };
  vector<string> children;
  node node;

  getline(line_parser, token, ' ');
  string name = token;
  getline(line_parser, token, ' ');
  int w = stoi(token.substr(1, token.length() - 2));
  node = make_pair(name, w);

  while (!line_parser.eof())
  {
    getline(line_parser, token, ' ');
    if (token[token.length() - 1] == ',')
    {
      token = token.substr(0, token.length() - 1);
    }
    if (token == "->")
    {
      continue;
    }
    children.push_back(token);
  }

  return make_pair(node, children);
}

string solve_first(istream& input){

  map<string, node> parents;
  string line;
  while (!input.eof())
  {
    getline(input, line);
    if (line == "")
    {
      continue;
    }
    auto node = parse(line);
    for (auto child : node.second)
    {
      parents[child] = node.first;
    }
  }

  auto node = parents.begin()->first;
  while (parents.find(node) != parents.end())
  {
    node = parents[node].first;
  }

	return node;
}

class WeightBank
{
  map<string, vector<string>>& children;
  map<string, int>& own_weights;
  map<string, int> total_weights;

public:
  WeightBank(map<string, vector<string>>& children, map<string, int>& weights)
    :children(children), own_weights(weights)
  {}

  int get_weight(string& node)
  {
    if (total_weights.find(node) != total_weights.end())
    {
      return total_weights[node];
    }
    int sum = own_weights[node];
    for (auto child : children[node])
    {
      sum += get_weight(child);
    }
    total_weights[node] = sum;
    return sum;
  }

};
void print_rec(string node, map<string, vector<string>>& children, WeightBank& bank, map<string, int>& weights, int depth)
{
  for (int i = 0; i < depth; i++)
  {
    cout << "    ";
  }
  cout << bank.get_weight(node) << " - " << node << "(" << weights[node] << ")" << endl;
  for (auto child : children[node])
  {
    print_rec(child, children, bank, weights, depth + 1);
  }
}

int solve_second(istream& input){

  map<string, vector<string>> children;
  map<string, int> weights;

  string line;
  while (!input.eof())
  {
    getline(input, line);
    if (line == "")
    {
      continue;
    }
    auto node = parse(line);
    children[node.first.first] = node.second;
    weights[node.first.first] = node.first.second;
  }

  WeightBank weight_bank(children, weights);

  input.clear();
  input.seekg(0, ios::beg);
  auto node = solve_first(input); // find the root element
  //print_rec(node, children, weight_bank, weights, 0);

  bool go_deeper;
  int diff = 0;
  while(true)
  {
    if (children[node].size() < 2)
    {
      break;
    }

    vector<string>& current_children = children[node];
    
    // sort children by total weight
    // if there is an unbalanced child it will be at the beginning or at the end
    sort(begin(current_children), end(current_children), [&weight_bank](string a, string b)
    {
      return weight_bank.get_weight(a) < weight_bank.get_weight(b);
    });

    // if the first and last weight are the same then all is the same, which means the "child towers" from now on are all balanced
    // the current node is the top unbalanced tower
    int last_index = current_children.size() - 1;
    if (weight_bank.get_weight(current_children[0]) == weight_bank.get_weight(current_children[last_index]))
    {
      return weights[node] - diff;
    }

    // if there is only two children, then use the previous difference to determine which "tower" is the unbalanced
    if (current_children.size() == 2)
    {
      auto actual_diff = weight_bank.get_weight(current_children[0]) - weight_bank.get_weight(current_children[last_index]);
      assert(diff == abs(actual_diff));
      if (diff == actual_diff)
      {
        node = current_children[0];
        continue;
      }
      if (diff == -actual_diff)
      {
        node = current_children[last_index];
        continue;
      }
    }

    // if the first and second element is the same then the last one is different
    if (weight_bank.get_weight(current_children[0]) == weight_bank.get_weight(current_children[1]))
    {
      node = current_children[last_index];
      diff = weight_bank.get_weight(node) - weight_bank.get_weight(current_children[1]);
      continue;
    }
    // if the last and second element is the same then the first one is different
    if (weight_bank.get_weight(current_children[last_index]) == weight_bank.get_weight(current_children[1]))
    {
      node = current_children[0];
      diff = weight_bank.get_weight(node) - weight_bank.get_weight(current_children[1]);
      continue;
    }
  }
    
  return -1;
}

