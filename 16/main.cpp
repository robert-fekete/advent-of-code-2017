#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <set>

using namespace std;

string solve_first(istream&, int);
string solve_second(istream&, int, long);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("s1,x3/4,pe/b"), 5) << endl;  // baedc
    cout << solve_second(stringstream("s1,x3/4,pe/b"), 5, 2) << endl;  // ceadb

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f, 16) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f, 16, 1000000000L) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

typedef vector<char> letters;
typedef function<void(letters&)> instruction;

void move_front(int move_number, letters& dancers)
{
  if (move_number == dancers.size())
  {
    return;
  }

  letters temp(move_number);
  for (int i = 0; i < move_number; i++)
  {
    auto index = dancers.size() - move_number + i;
    temp[i] = dancers[index];
  }
  for (int i = dancers.size() - move_number - 1; i >= 0; i--)
  {
    dancers[i + move_number] = dancers[i];
  }

  for (int i = 0; i < move_number; i++)
  {
    dancers[i] = temp[i];
  }
}

void swap_by_index(int a, int b, letters& dancers)
{
  swap(dancers[a], dancers[b]);
}

void swap_by_value(char a, char b, letters& dancers)
{
  auto index_a = find(begin(dancers), end(dancers), a);
  auto index_b = find(begin(dancers), end(dancers), b);

  swap(*index_a, *index_b);
}

instruction parse_line(string line)
{
  if (line[0] == 's')
  {
    auto move_number = stoi(line.substr(1, line.size() - 1));
    return [move_number](letters& dancers)
    {
      move_front(move_number, dancers);
    };
  }
  auto values = line.substr(1, line.size() - 1);
  stringstream line_parser{ values };

  string first;
  string second;

  getline(line_parser, first, '/');
  getline(line_parser, second, '/');
  if (line[0] == 'x')
  {
    return [first, second](letters& dancers)
    {
      swap_by_index(stoi(first), stoi(second), dancers);
    };
  }
  if (line[0] == 'p')
  {
    return [first, second](letters& dancers)
    {
      swap_by_value(first[0], second[0], dancers);
    };
  }

  throw logic_error("Unkown instruction");
}

vector<instruction> parse(istream& input)
{
  string line;
  vector<instruction> instructions;
  int count = 0;
  while (!input.eof())
  {
    getline(input, line, ',');
    if (line != "")
    {
      auto instruction = parse_line(line);
      instructions.push_back(instruction);
    }
  }
  return instructions;
}


string dance(vector<instruction>& instructions, letters& dancers)
{
  for (auto instruction : instructions)
  {
    instruction(dancers);
  }
  

  stringstream result;
  for (auto dancer : dancers)
  {
    result << dancer;
  }

  return result.str();
}

string solve_first(istream& input, int number_of_letters){

  letters dancers(number_of_letters);
  for (int i = 0; i < number_of_letters; i++)
  {
    dancers[i] = 'a' + i;
  }

  auto instructions = parse(input);
  auto result = dance(instructions, dancers);

  return result;
}

string solve_second(istream& input, int number_of_letters, long number_of_runs)
{
  auto instructions = parse(input);

  letters dancers(number_of_letters);
  for (int i = 0; i < number_of_letters; i++)
  {
    dancers[i] = 'a' + i;
  }

  set<string> pattern_occurrence;
  vector<string> patterns;
  int patterns_found = -1;
  while(patterns_found != pattern_occurrence.size())
  {
    patterns_found = pattern_occurrence.size();
    auto pattern = dance(instructions, dancers);
    pattern_occurrence.insert(pattern);
    patterns.push_back(pattern);
  }

  int remainder = number_of_runs % patterns_found;
  remainder--; // starting pattern is not included in the patterns vector

  return patterns[remainder];
}

