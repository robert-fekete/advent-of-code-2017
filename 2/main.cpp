#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("5\t1\t9\t5\n7\t5\t3\n2\t4\t6\t8\n")) << endl;  // 18
    cout << solve_second(stringstream("5\t9\t2\t8\n9\t4\t7\t3\n3\t8\t6\t5")) << endl;  // 9

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

int sum_of_spreadsheet(istream& input, function<int(vector<int>&)> predicate)
{
  string line;
  auto sum = unsigned short{ 0 };
  while (!input.eof())
  {
    getline(input, line);
    if (line == "")
    {
      continue;
    }

    auto line_parser = stringstream{ line };
    string token;

    vector<int> numbers;
    while (!line_parser.eof())
    {
      getline(line_parser, token, '\t');
      if (token != "")
      {
        numbers.push_back(stoi(token));
      }
    }

    sum += predicate(numbers);
  }
  return sum;
}

int solve_first(istream& input)
{
  return sum_of_spreadsheet(input, [](vector<int> &numbers)
  {
    auto max = *max_element(begin(numbers), end(numbers));
    auto min = *min_element(begin(numbers), end(numbers));

    return max - min;
  });
}

int solve_second(istream& input)
{
  return sum_of_spreadsheet(input, [](vector<int> &numbers)
  {
    for (int i = 0; i < numbers.size(); i++)
    {
      for (int j = 0; j < numbers.size(); j++)
      {
        if (i == j)
        {
          continue;
        }

        if (numbers[i] % numbers[j] == 0)
        {
          return numbers[i] / numbers[j];
        }
      }
    }

    throw logic_error("No evenly divisible values found");
  });
}

