﻿using System.Collections.Generic;

namespace _23.Assembly
{
    internal class Processor
    {
        private readonly Registry registry;
        private readonly IList<IInstruction> instructions;
        private readonly bool verbose;

        private int instructionPointer = 0;

        public Processor(Registry registry, IList<IInstruction> instructions, bool verbose)
        {
            this.registry = registry;
            this.instructions = instructions;
            this.verbose = verbose;
        }

        public long Execute()
        {
            long counter = 0;
            while (instructionPointer < instructions.Count)
            {
                if (verbose)
                {
                    System.Console.WriteLine($"Executing line {instructionPointer}.");
                    registry.Print();
                }
                instructions[instructionPointer].Execute(registry, ref instructionPointer, ref counter);
                instructionPointer++;
            }

            return counter;
        }
    }
}
