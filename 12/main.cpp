#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <stack>
#include <set>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
  cout << solve_first(stringstream("0 <-> 2\n1 <-> 1\n2 <-> 0, 3, 4\n3 <-> 2, 4\n4 <-> 2, 3, 6\n5 <-> 6\n6 <-> 4, 5")) << endl;  // 6
  cout << solve_second(stringstream("0 <-> 2\n1 <-> 1\n2 <-> 0, 3, 4\n3 <-> 2, 4\n4 <-> 2, 3, 6\n5 <-> 6\n6 <-> 4, 5")) << endl;  // 2

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

map<int, vector<int>> parse(istream& input)
{
  string line;
  map<int, vector<int>> nodes;
  while (!input.eof())
  {
    getline(input, line);
    if (line == "")
    {
      continue;
    }
    string token;
    stringstream line_parser{ line };
    getline(line_parser, token, ' ');
    int current_node = stoi(token);
    getline(line_parser, token, ' ');
    vector<int> neighbours;
    while (!line_parser.eof())
    {
      getline(line_parser, token, ' ');
      if (token != "")
      {
        if (token[token.length() - 1] == ',')
        {
          token = token.substr(0, token.length() - 1);
        }
        neighbours.push_back(stoi(token));
      }
    }
    nodes[current_node] = neighbours;
  }
  return nodes;
}

class componentcounter
{
  map<int, vector<int>>& nodes;
  set<int> visited;
public:

  componentcounter(map<int, vector<int>>& nodes)
    :nodes(nodes)
  {}

  int count(int starting_node)
  {
    int count = 0;
    stack<int> next_nodes;
    next_nodes.push(starting_node);
    while (next_nodes.size() > 0)
    {
      int current_node = next_nodes.top();
      next_nodes.pop();
      if (visited.find(current_node) != visited.end())
      {
        continue;
      }
      visited.insert(current_node);
      count++;
      for (auto next : nodes[current_node])
      {
        next_nodes.push(next);
      }
    }
    return count;
  }
};

int solve_first(istream& input){

  auto nodes = parse(input);
  auto counter = componentcounter(nodes);
  auto result = counter.count(0);
	return result;
}

int solve_second(istream& input){

  auto nodes = parse(input);
  int result = 0;
  auto counter = componentcounter(nodes);
  for (auto node : nodes)
  {
    auto group_size = counter.count(node.first);
    if (group_size > 0)
    {
      result++;
    }
  }
  return result;
}

