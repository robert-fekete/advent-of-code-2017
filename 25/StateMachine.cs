﻿using System.Collections.Generic;

namespace _25
{
    internal class StateMachine
    {
        private readonly Dictionary<string, State> states;
        private readonly Cursor cursor;

        private string currentState;

        public StateMachine(Dictionary<string, State> states, string startingState, Cursor cursor)
        {
            this.states = states;
            currentState = startingState;
            this.cursor = cursor;
        }

        public int Run(int numberOfIterations)
        {
            for (int i = 0; i < numberOfIterations; i++)
            {
                currentState = states[currentState].Execute(cursor);
            }

            return cursor.CheckSum;
        }
    }
}