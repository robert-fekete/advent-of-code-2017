﻿using System;
using System.Collections.Generic;

namespace _22
{
    internal class Worm
    {
        private Direction direction;
        private readonly Dictionary<Direction, Coordinate> offsets = new Dictionary<Direction, Coordinate>
        {
            { Direction.Top, new Coordinate(0, -1) },
            { Direction.Right, new Coordinate(1, 0) },
            { Direction.Bottom, new Coordinate(0, 1) },
            { Direction.Left, new Coordinate(-1, 0) },
        };

        private Worm(Coordinate position)
        {
            Position = position;
            direction = Direction.Top;
        }

        public Coordinate Position { get; private set; }

        internal static Worm FromInput(string input)
        {
            var firstLine = input.Split(Environment.NewLine)[0].Trim();
            var middle = firstLine.Length / 2;

            return new Worm(new Coordinate(middle, middle));
        }

        internal void TurnLeft()
        {
            switch (direction)
            {
                case Direction.Top:
                    direction = Direction.Left;
                    break;
                case Direction.Right:
                    direction = Direction.Top;
                    break;
                case Direction.Bottom:
                    direction = Direction.Right;
                    break;
                case Direction.Left:
                    direction = Direction.Bottom;
                    break;
            }
        }

        internal void TurnRight()
        {
            switch (direction)
            {
                case Direction.Top:
                    direction = Direction.Right;
                    break;
                case Direction.Right:
                    direction = Direction.Bottom;
                    break;
                case Direction.Bottom:
                    direction = Direction.Left;
                    break;
                case Direction.Left:
                    direction = Direction.Top;
                    break;
            }
        }

        internal void Reverse()
        {
            switch (direction)
            {
                case Direction.Top:
                    direction = Direction.Bottom;
                    break;
                case Direction.Right:
                    direction = Direction.Left;
                    break;
                case Direction.Bottom:
                    direction = Direction.Top;
                    break;
                case Direction.Left:
                    direction = Direction.Right;
                    break;
            }
        }

        internal void Move()
        {
            Position += offsets[direction];
        }

        private enum Direction
        {
            Top,
            Right,
            Bottom,
            Left
        }
    }
}