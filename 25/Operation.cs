﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _25
{
    internal class Operation
    {
        private readonly bool writeValue;
        private readonly int moveOffset;
        private readonly string nextState;

        public Operation(bool writeValue, int moveOffset, string nextState)
        {
            this.writeValue = writeValue;
            this.moveOffset = moveOffset;
            this.nextState = nextState;
        }

        public string Execute(Cursor cursor)
        {
            cursor.Value = writeValue;
            cursor.Move(moveOffset);

            return nextState;
        }
    }
}
