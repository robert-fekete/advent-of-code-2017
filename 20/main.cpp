#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <unordered_map>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("p=<3,0,0>, v=<2,0,0>, a=<-1,0,0>\np=<4,0,0>, v=<0,0,0>, a=<-2,0,0>")) << endl;  // 0
    cout << solve_second(stringstream("p=<-6,0,0>, v=<3,0,0>, a=<0,0,0>\np=<-4,0,0>, v=<2,0,0>, a=<0,0,0>\np=<-2,0,0>, v=<1,0,0>, a=<0,0,0>\np=<3,0,0>, v=<-1,0,0>, a=<0,0,0>")) << endl;  // 1

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

struct vector3
{
  int x;
  int y;
  int z;

  vector3()
    :vector3(0, 0, 0)
  {}

  vector3(int x, int y, int z)
    : x(x), y(y), z(z)
  {}

  vector3 operator+(vector3 other)
  {
    return vector3(x + other.x, y + other.y, z + other.z);
  }

  void operator+=(vector3 other)
  {
    x += other.x;
    y += other.y;
    z += other.z;
  }
};

class particle
{
  vector3 position;
  vector3 velocity;
  vector3 acceleration;
  int id;

public:

  particle()
    : particle(vector3(), vector3(), vector3(), 0)
  {}

  particle(vector3 position, vector3 velocity, vector3 acceleration, int id)
    : position(position),
    velocity(velocity),
    acceleration(acceleration),
    id(id)
  {}

  vector3 get_acceleration()
  {
  return acceleration;
  }

  void update()
  {
    velocity += acceleration;
    position += velocity;
  }

  vector3 get_position()
  {
    return position;
  }

  int get_id()
  {
    return id;
  }
};

vector3 parse_vector(string vector)
{
  stringstream  vector_parser{ vector };
  string token;
  getline(vector_parser, token, '<');

  getline(vector_parser, token, ',');
  int x = stoi(token);
  getline(vector_parser, token, ',');
  int y = stoi(token);
  getline(vector_parser, token, '>');
  int z = stoi(token);

  return vector3(x, y, z);
}

particle parse_line(string line, int id)
{
  stringstream  line_parser{ line };
  string token;

  getline(line_parser, token, ' ');
  auto pos = parse_vector(token);
  getline(line_parser, token, ' ');
  auto vel = parse_vector(token);
  getline(line_parser, token, ' ');
  auto acc = parse_vector(token);

  return particle(pos, vel, acc, id);
}

vector<particle> parse(istream& input)
{
  string line;
  vector<particle> particles;
  int i = 0;
  while (!input.eof())
  {
    getline(input, line);
    if (line != "")
    {
      auto particle = parse_line(line, i);
      particles.push_back(particle);
      i++;
    }
  }

  return particles;
}

int vector_length(vector3& vec)
{
  return vec.x * vec.x + vec.y * vec.y + vec.z * vec.z;
}

int solve_first(istream& input)
{

  auto particles = parse(input);
  sort(begin(particles), end(particles), [](particle a, particle b) { return vector_length(a.get_acceleration()) < vector_length(b.get_acceleration()); });

  return particles[0].get_id();
}

int solve_second(istream& input)
{
  
    map<int, particle> remaining;
    auto particles = parse(input);
    for (auto particle : particles)
    {
      remaining[particle.get_id()] = particle;
    }

    int time_since_collision = 0;
    int simulation_steps = 0;
    while (time_since_collision < 10)
    {
      simulation_steps++;
      // grouping by position
      map<pair<int, pair<int, int>>, vector<particle>> groups;
      for (auto& p : remaining)
      {
        p.second.update();
        auto pos = p.second.get_position();
        auto key = make_pair(pos.x, make_pair(pos.y, pos.z));
        if (groups.find(key) == groups.end())
        {
          groups[key] = vector<particle>();
        }
        groups[key].push_back(p.second);
      }

      for (auto g : groups)
      {
        // collision if two particles at the same position -> group
        if (g.second.size() > 1)
        {
          for (auto p : g.second)
          {
            remaining.erase(p.get_id());
          }
          time_since_collision = 0;
        }
      }

      time_since_collision++;
    }
    cout << "Simulation took " << simulation_steps << " steps" << endl;
    return remaining.size();
}
