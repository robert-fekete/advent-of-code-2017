﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _22
{
    internal class GridMap
    {
        private readonly HashSet<Coordinate> infestedCells;
        private readonly HashSet<Coordinate> weakendCells = new HashSet<Coordinate>();
        private readonly HashSet<Coordinate> flaggedCells = new HashSet<Coordinate>();
        public GridMap(IEnumerable<Coordinate> initialInfestedCells)
        {
            infestedCells = new HashSet<Coordinate>(initialInfestedCells);
        }

        public static GridMap FromInput(string input)
        {
            var infestedCells = new List<Coordinate>();
            var lines = input.Split(Environment.NewLine).Select(l => l.Trim());
            var lineIndex = 0;
            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }

                for(var columnIndex = 0; columnIndex < line.Length; columnIndex++)
                {
                    if(line[columnIndex] == '#')
                    {
                        infestedCells.Add(new Coordinate(columnIndex, lineIndex));
                    }
                }
                lineIndex++;
            }

            return new GridMap(infestedCells);
        }

        internal bool IsInfested(Coordinate coordinate)
        {
            return infestedCells.Contains(coordinate);
        }

        internal bool IsWeakend(Coordinate coordinate)
        {
            return weakendCells.Contains(coordinate);
        }

        internal bool IsFlagged(Coordinate coordinate)
        {
            return flaggedCells.Contains(coordinate);
        }

        internal void Clean(Coordinate coordinate)
        {
            if (infestedCells.Contains(coordinate))
            {
                infestedCells.Remove(coordinate);
            }
            if (flaggedCells.Contains(coordinate))
            {
                flaggedCells.Remove(coordinate);
            }
            if (weakendCells.Contains(coordinate))
            {
                weakendCells.Remove(coordinate);
            }
        }

        internal void Infest(Coordinate coordinate)
        {
            Clean(coordinate);
            infestedCells.Add(coordinate);
        }

        internal void Weaken(Coordinate coordinate)
        {
            Clean(coordinate);
            weakendCells.Add(coordinate);
        }

        internal void Flag(Coordinate coordinate)
        {
            Clean(coordinate);
            flaggedCells.Add(coordinate);
        }

        private int noPrints = 0;
        internal void Print(int top, int left, int width, int height)
        {
            Console.WriteLine($"{noPrints++}. step");
            for(int y = top; y < top + height; y++)
            {
                for(int x = left; x < left + width; x++)
                {
                    if (IsInfested(new Coordinate(x, y)))
                    {
                        Console.Write('#');
                    }
                    else
                    {
                        Console.Write('.');
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
