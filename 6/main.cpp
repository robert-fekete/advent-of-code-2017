#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <set>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
  cout << solve_first(stringstream("0\t2\t7\t0\n")) << endl;  // 5
  cout << solve_second(stringstream("0\t2\t7\t0\n")) << endl;  // 4

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

class MemoryBankManager
{
  vector<int> banks;
  int size;

public:
  MemoryBankManager(vector<int> banks)
    : banks(banks), size(banks.size())
  {}

  void reallocate()
  {
    auto largest_bank_index = get_start_index();
    auto bank_value = banks[largest_bank_index];
    auto full_rounds = bank_value / size;
    auto leftover = bank_value % size;

    banks[largest_bank_index] = 0;
    for (int i = 0; i < size; i++)
    {
      banks[i] += full_rounds;
      int a = mod((i - largest_bank_index - 1));
      if (a < leftover)
      {
        banks[i] += 1;
      }
    }
  }

  vector<int> data()
  {
    return banks;
  }

  void print()
  {
    for (auto bank : banks)
    {
      cout << bank << " ";
    }
    cout << endl;
  }

private:
  int get_start_index()
  {
    int max_value = banks[0];
    int max_index = 0;
    for (int i = 0; i < size; i++)
    {
      if (banks[i] > max_value)
      {
        max_value = banks[i];
        max_index = i;
      }
    }

    return max_index;
  }

  int mod(int num)
  {
    while (num < 0)
    {
      num += size;
    }
    while (num >= size)
    {
      num -= size;
    }
    return num;
  }
};

class PatternMatcher
{
  set<vector<int>> patterns;

public:

  bool add_pattern(vector<int> pattern)
  {
    int size = patterns.size();
    patterns.insert(pattern);

    return size == patterns.size();
  }
};

vector<int> parse_banks(istream& input)
{
  string line;
  getline(input, line);
  string token;
  auto line_parser = stringstream{ line };
  vector<int> banks;
  while (!line_parser.eof())
  {
    getline(line_parser, token, '\t');
    if (token != "")
    {
      banks.push_back(stoi(token));
    }
  }

  return banks;
}

int solve_first(istream& input){

  auto banks = parse_banks(input);

  auto bank_manager = MemoryBankManager(banks);
  auto pattern_matcher = PatternMatcher();
  int count = 0;
  while (!pattern_matcher.add_pattern(bank_manager.data()))
  {
    bank_manager.reallocate();
    count++;
  }

	return count;
}

int solve_second(istream& input){

  auto banks = parse_banks(input);

  auto bank_manager = MemoryBankManager(banks);
  auto pattern_matcher = PatternMatcher();
  while (!pattern_matcher.add_pattern(bank_manager.data()))
  {
    bank_manager.reallocate();
  }

  int count = 0;
  pattern_matcher = PatternMatcher();
  while (!pattern_matcher.add_pattern(bank_manager.data()))
  {
    bank_manager.reallocate();
    count++;
  }

  return count;
}

