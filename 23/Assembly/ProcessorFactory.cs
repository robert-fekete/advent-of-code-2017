﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _23.Assembly
{
    internal class ProcessorFactory
    {
        private readonly Func<string, IInstruction> parsingMethod;

        public ProcessorFactory(Func<string, IInstruction> parsingMethod)
        {
            this.parsingMethod = parsingMethod;
        }

        public Processor Create(string input, IDictionary<string, int> registerNames, bool verbose)
        {
            var lines = input.Split(Environment.NewLine).Select(l => l.Trim());
            var instructions = new List<IInstruction>();

            foreach(var line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }

                var instruction = parsingMethod(line);
                instructions.Add(instruction);
            }

            var registry = new Registry(registerNames);
            var processor = new Processor(registry, instructions, verbose);

            return processor;
        }
    }
}
