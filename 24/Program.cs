﻿using System;

namespace _24
{
    class Program
    {
        static void Main(string[] args)
        {
            var challenge = new Challenge24();
            challenge.Run();
        }
    }
}
