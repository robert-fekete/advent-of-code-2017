﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _24
{
    internal class Traversal
    {
        public int GetMaxStrengthSubTreeSum(IEnumerable<BridgeComponent> nodes)
        {
            var availableNodes = new HashSet<BridgeComponent>(nodes);
            (var strength, var length) = GetMaxSubTreeSumRec(MaxStrengthPredicate, new BridgeComponent(0, 0), availableNodes);

            return strength;
        }

        public int GetMaxLengthSubTreeSum(IEnumerable<BridgeComponent> nodes)
        {
            var availableNodes = new HashSet<BridgeComponent>(nodes);
            (var strength, var length) = GetMaxSubTreeSumRec(MaxLengthPredicate, new BridgeComponent(0, 0), availableNodes);

            return strength;
        }

        private (int, int) GetMaxSubTreeSumRec(Func<(int Strength, int Length), (int Strength, int Length), (int, int)> predicate, BridgeComponent node, HashSet<BridgeComponent> availableNodes)
        {
            if (availableNodes.Count == 0)
            {
                return (node.Strength, 1);
            }

            var maxStrength = 0;
            var maxLength = 0;
            foreach(var next in availableNodes.ToArray())
            {
                if (node.CanContinueWith(next))
                {
                    next.Take(node.FreePort);
                    availableNodes.Remove(next);

                    var result = GetMaxSubTreeSumRec(predicate, next, availableNodes);

                    (maxStrength, maxLength) = predicate((maxStrength, maxLength), result);
                    
                    availableNodes.Add(next);
                    next.ReleaseAll();
                }
            }

            return (maxStrength + node.Strength, maxLength + 1);
        }

        private (int, int) MaxStrengthPredicate((int Strength, int Length) max, (int Strength, int Length) current)
        {
            if (max.Strength < current.Strength)
            {
                return current;
            }
            return max;
        }

        private (int, int) MaxLengthPredicate((int Strength, int Length) max, (int Strength, int Length) current)
        {
            if (max.Length < current.Length)
            {
                return current;
            }
            if (max.Length == current.Length && max.Strength < current.Strength)
            {
                return current;
            }
            return max;
        }
    }
}
