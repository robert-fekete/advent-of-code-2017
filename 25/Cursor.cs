﻿using System;
using System.Collections.Generic;

namespace _25
{
    public class Cursor
    {
        private int currentPosition = 0;
        private HashSet<int> ones = new HashSet<int>();

        public int CheckSum => ones.Count;
        public bool Value
        {
            get
            {
                return ones.Contains(currentPosition);
            }
            set
            {

                if (value == ones.Contains(currentPosition))
                {
                    return;
                }

                if (value)
                {
                    ones.Add(currentPosition);
                }
                else
                {
                    ones.Remove(currentPosition);
                }
            }
        }

        // Moving right is a positive offset
        // Moving left is a negative offset
        internal void Move(int moveOffset)
        {
            currentPosition += moveOffset;
        }
    }
}