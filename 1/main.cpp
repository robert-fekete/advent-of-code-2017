#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

unsigned int solve_first(istream&);
unsigned int solve_second(istream&);

int main(int argc, char* argv[])
{
  cout << solve_first(stringstream("1122")) << endl;  // 3
  cout << solve_first(stringstream("1111")) << endl;  // 4
  cout << solve_first(stringstream("1234")) << endl;  // 0
  cout << solve_first(stringstream("91212129")) << endl;  // 9
  cout << solve_second(stringstream("1212")) << endl;  // 6
  cout << solve_second(stringstream("1221")) << endl;  // 0
  cout << solve_second(stringstream("123425")) << endl;  // 4
  cout << solve_second(stringstream("123123")) << endl;  // 12
  cout << solve_second(stringstream("12131415")) << endl;  // 4

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

unsigned int count_with_offset(string &line, unsigned short offset)
{
  unsigned int sum = 0;

  for (auto i = 0; i <= line.length(); i++)
  {
    auto next = (i + offset) % line.length();
    if (line[i] == line[next])
    {
      sum += (unsigned int)(line[i] - '0');
    }
  }
  return sum;
}

unsigned int solve_first(istream& input){

  string line;
  input >> line;
  
  return count_with_offset(line, 1);
}

unsigned int solve_second(istream& input){

  string line;
  input >> line;

  return count_with_offset(line, line.length() / 2);
}

