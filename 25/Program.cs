﻿using System;
using System.IO;

namespace _25
{
    class Program
    {
        static void Main(string[] args)
        {
            var challenge = new Challenge25();
            challenge.Run();
        }
    }
}
