#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

string solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("     |          \n     |  +--+    \n     A  |  C    \n F---|----E|--+ \n     |  |  |  D \n     +B-+  +--+ \n\n")) << endl;  // ABCDEF
    cout << solve_second(stringstream("     |          \n     |  +--+    \n     A  |  C    \n F---|----E|--+ \n     |  |  |  D \n     +B-+  +--+ \n\n")) << endl;  // 38

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

enum directions
{
  UP = 0,
  DOWN = 1,
  LEFT = 2,
  RIGHT = 3
};

struct coordinate
{
  int x;
  int y;

  coordinate()
    :coordinate(0, 0)
  {}

  coordinate(int x, int y)
    : x(x), y(y)
  {}
};

class network
{
  vector<string>& rows;
  directions direction;
  coordinate position;

public:
  network(vector<string>& rows)
    : rows(rows),
    direction(directions::DOWN)
  {
    find_starting_position();
  }

  bool finished()
  {
    return !is_step_valid(direction);
  }

  char get_next()
  {
    position = get_next_step(direction);
    auto next = rows[position.y][position.x];
    if (next == '+')
    {
      change_direction();
    }
    return next;
  }

private:
  
  void find_starting_position()
  {
    for (int i = 0; i < rows[0].size(); i++)
    {
      if (rows[0][i] == '|')
      {
        position = coordinate(i, 0);
        return;
      }
    }
  }

  coordinate get_next_step(directions target_direction)
  {
    switch (target_direction)
    {
    case directions::DOWN:
      return coordinate(position.x, position.y + 1);
    case directions::UP:
      return coordinate(position.x, position.y - 1);
    case directions::LEFT:
      return coordinate(position.x - 1, position.y);
    case directions::RIGHT:
      return coordinate(position.x + 1, position.y);
    }
  }

  bool is_step_valid(directions target_direction)
  {
    switch (target_direction)
    {
    case directions::DOWN:
      return position.y + 1 < rows.size() && rows[position.y + 1][position.x] != ' ';
    case directions::UP:
      return position.y - 1 >=0  && rows[position.y - 1][position.x] != ' ';
    case directions::LEFT:
      return position.x - 1 >= 0 && rows[position.y][position.x - 1] != ' ';
    case directions::RIGHT:
      return position.x + 1 < rows[position.y].size() && rows[position.y][position.x + 1] != ' ';
    }
  }

  void change_direction()
  {
    if (direction != directions::DOWN && is_step_valid(directions::UP))
    {
      direction = directions::UP;
      return;
    }
    if (direction != directions::UP && is_step_valid(directions::DOWN))
    {
      direction = directions::DOWN;
      return;
    }
    if (direction != directions::RIGHT && is_step_valid(directions::LEFT))
    {
      direction = directions::LEFT;
      return;
    }
    if (direction != directions::LEFT && is_step_valid(directions::RIGHT))
    {
      direction = directions::RIGHT;
      return;
    }
  }
};

class traverser
{
  network& path;
  function<void(char c)> action;

public:
  traverser(network& maze, function<void(char c)> action)
    :path(maze),
    action(action)
  { }

  void go()
  {
    while (!path.finished())
    {
      action(path.get_next());
    }
  }
};

vector<string> parse(istream& input)
{
  vector<string> rows;
  string line;
  while (!input.eof())
  {
    getline(input, line);
    if (line != "")
    {
      rows.push_back(line);
    }
  }
  return rows;
}

string solve_first(istream& input){

  auto lines = parse(input);
  network network{ lines };
  stringstream result;
  traverser traverser{ network,[&result](char c)
  {
    if (c != '+' && c != '|' && c != '-')
    {
      result << c;
    }
  } };

  traverser.go();

	return result.str();
}

int solve_second(istream& input){

  auto lines = parse(input);
  network network{ lines };
  int steps = 1;
  traverser traverser{ network,[&steps](char c)
  {
    steps++;
  } };

  traverser.go();

  return steps;
}

