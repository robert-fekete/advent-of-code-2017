#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <queue>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("set a 1\nadd a 2\nmul a a\nmod a 5\nsnd a\nset a 0\nrcv a\njgz a 4\njgz a -2\nset a 1\njgz a -2")) << endl;  // 4
    //cout << solve_second(stringstream("snd 1\nsnd 2\nsnd p\nrcv a\nrcv b\nrcv c")) << endl;  // 3
    //cout << solve_second(stringstream("set a 1\nadd a 2\nmul a a\nmod a 5\nsnd a\nset a 0\nrcv a\njgz a -1\nset a 1\njgz a -2")) << endl;  // 1
    //cout << solve_second(stringstream("set a 1\nset b 4\nadd a 2\nmul a a\nmod a 5\nsnd a\nset a 0\nrcv a\nsnd b\nadd b -1\nrcv a\njgz b -3\nset a 1\njgz b -2")) << endl;  // 5

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl; // 7857 > x > 5226
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}
class processor;
typedef function<void(processor&)> instruction;

typedef long long register_t;
class registers
{
  map<char, register_t> values;
public:
  registers()
  {}

  register_t get(char c)
  {
    if (c < 'a' || c > 'z')
    {
      throw logic_error("Invalid registry key");
    }

    auto result = values.find(c);
    if (result == values.end())
    {
      return 0;
    }

    return result->second;
  }

  void set(char c, register_t i)
  {
    if (c < 'a' || c > 'z')
    {
      throw logic_error("Invalid registry key");
    }
    values[c] = i;
  }
};

class processor
{
  vector<instruction>& instructions;

protected:

  registers registers;
  int instruction_pointer;
  bool changed = true;

public:

  processor(vector<instruction>& instructions)
    : instruction_pointer(0),
    instructions(instructions)
  {}

  void start()
  {
    instruction_pointer = 0;
    init();
    run();
  }

  bool is_changed()
  {
    return changed;
  }

  virtual bool is_terminated()
  {
    return instruction_pointer < 0 || instruction_pointer >= instructions.size();
  }

  virtual void snd(string arg) = 0;

  virtual void rcv(string arg) = 0;

  void set(string arg1, string arg2)
  {
    try
    {
      set(arg1[0], stoll(arg2));
    }
    catch (invalid_argument&)
    {
      set(arg1[0], arg2[0]);
    }
  }

  void add(string arg1, string arg2)
  {
    try
    {
      add(arg1[0], stoll(arg2));
    }
    catch (invalid_argument&)
    {
      add(arg1[0], arg2[0]);
    }
  }

  void mul(string arg1, string arg2)
  {
    try
    {
      mul(arg1[0], stoll(arg2));
    }
    catch (invalid_argument&)
    {
      mul(arg1[0], arg2[0]);
    }
  }

  void mod(string arg1, string arg2)
  {
    try
    {
      mod(arg1[0], stoll(arg2));
    }
    catch (invalid_argument&)
    {
      mod(arg1[0], arg2[0]);
    }
  }

  void jump(string arg1, string arg2)
  {
    register_t first;
    register_t second;

    bool is_first_number = true;
    bool is_second_number = true;

    try
    {
      first = stoll(arg1);
    }
    catch (invalid_argument&)
    {
      is_first_number = false;
    }

    try
    {
      second = stoll(arg2);
    }
    catch (invalid_argument&)
    {
      is_second_number = false;
    }

    if (is_first_number && is_second_number)
    {
      jump(first, second);
    }
    else if (is_second_number)
    {
      jump(arg1[0], second);
    }
    else if (is_first_number)
    {
      jump(first, arg2[0]);
    }
    else
    {
      jump(arg1[0], arg2[0]);
    }
  }

protected:

  void run()
  {
    int temp = instruction_pointer;
    int number_of_cycles = 0;
    while (!is_terminated())
    {
      instructions[instruction_pointer](*this);
      instruction_pointer++;
      number_of_cycles++;
    }
    changed = !(temp == instruction_pointer && number_of_cycles == 1);
  }

  virtual void init() = 0;

private:

  void set(char x, register_t y)
  {
    registers.set(x, y);
  }

  void set(char x, char y)
  {
    set(x, registers.get(y));
  }

  void add(char x, register_t y)
  {
    registers.set(x, registers.get(x) + y);
  }

  void add(char x, char y)
  {
    add(x, registers.get(y));
  }

  void mul(char x, register_t y)
  {
    registers.set(x, registers.get(x) * y);
  }

  void mul(char x, char y)
  {
    mul(x, registers.get(y));
  }

  void mod(char x, register_t y)
  {
    auto mod = registers.get(x) % y;
    registers.set(x, mod);
  }

  void mod(char x, char y)
  {
    mod(x, registers.get(y));
  }

  void jump(register_t x, register_t y)
  {
    if (x > 0)
    {
      instruction_pointer += y;
      instruction_pointer--; // preventing the upcoming instruction pointer increment
    }
  }

  void jump(char x, register_t y)
  {
    jump(registers.get(x), y);
  }

  void jump(register_t x, char y)
  {
    jump(x, registers.get(y));
  }

  void jump(char x, char y)
  {
    jump(registers.get(x), registers.get(y));
  }
};

class soundprocessor
  : public processor
{
  register_t last_sound;
  register_t recovered_sound;
  bool recover_found = false;

public:

  soundprocessor(vector<instruction>& instructions)
    :processor(instructions)
  {}

  virtual void snd(string arg)
  {
    try
    {
      send(stoll(arg));
    }
    catch (invalid_argument&)
    {
      send(arg[0]);
    }
  }

  virtual void rcv(string arg)
  {
    try
    {
      recover(stoll(arg));
    }
    catch (invalid_argument&)
    {
      recover(arg[0]);
    }
  }

  register_t get_last_recovered()
  {
    return recovered_sound;
  }

private:

  void init()
  {
    recover_found = false;
  }

  bool is_terminated()
  {
    return processor::is_terminated() || recover_found;
  }

  void send(register_t x)
  {
    //cout << "Beep: " << x << endl;
    last_sound = x;
  }

  void send(char x)
  {
    send(registers.get(x));
  }

  void recover(register_t x)
  {
    if (x != 0)
    {
      recovered_sound = last_sound;
      recover_found = true;
    }
  }

  void recover(char x)
  {
    recover(registers.get(x));
  }
};

class duetprocessor
  : public processor
{
  queue<register_t>& own_queue;
  queue<register_t>& other_queue;
  long long number_of_sends = 0;
  bool waiting = false;
  register_t id;

public:

  duetprocessor(register_t id, queue<register_t>& own, queue<register_t>& other, vector<instruction>& instructions)
    :processor(instructions),
    own_queue(own),
    other_queue(other),
    id(id)
  {
    registers.set('p', id);
  }

  void notify()
  {
    changed = false;
    waiting = false;
    run();
  }

  virtual void snd(string arg)
  {
    try
    {
      send(stoll(arg));
    }
    catch (invalid_argument&)
    {
      send(arg[0]);
    }
  }

  virtual void rcv(string arg)
  {
    try
    {
      receive(stoll(arg));
    }
    catch (invalid_argument&)
    {
      receive(arg[0]);
    }
  }

  long long get_number_of_sends()
  {
    return number_of_sends;
  }

  bool is_terminated()
  {
    return processor::is_terminated() || waiting;
  }

private:

  void init()
  {
    number_of_sends = 0;
  }

  void send(register_t x)
  {
    other_queue.push(x);
    //cout << "Process " << id << " sent: " << x << endl;
    number_of_sends++;
  }

  void send(char x)
  {
    send(registers.get(x));
  }

  void receive(register_t x)
  {
    throw logic_error("Can't store received value");
  }

  void receive(char x)
  {
    if (own_queue.size() > 0)
    {
      registers.set(x, own_queue.front());
      //cout << "Process " << id << " received: " << x << "= " << own_queue.front() << endl;
      own_queue.pop();
    }
    else
    {
      waiting = true;
      instruction_pointer--; // undoing the following increment
    }
  }
};

instruction parse_line(string line)
{
  string token;
  stringstream line_parser{ line };

  getline(line_parser, token, ' ');
  string instruction = token;

  getline(line_parser, token, ' ');
  string first_argument = token;

  if (instruction == "rcv")
  {
    return [first_argument](processor& proc)
    {
      proc.rcv(first_argument);
    };
  }
  if (instruction == "snd")
  {
    return [first_argument](processor& proc)
    {
      proc.snd(first_argument);
    };
  }

  getline(line_parser, token, ' ');
  string second_argument = token;

  if (instruction == "set")
  {
    return [first_argument, second_argument](processor& proc)
    {
      proc.set(first_argument, second_argument);
    };
  }
  if (instruction == "add")
  {
    return [first_argument, second_argument](processor& proc)
    {
      proc.add(first_argument, second_argument);
    };
  }
  if (instruction == "mul")
  {
    return [first_argument, second_argument](processor& proc)
    {
      proc.mul(first_argument, second_argument);
    };
  }
  if (instruction == "mod")
  {
    return [first_argument, second_argument](processor& proc)
    {
      proc.mod(first_argument, second_argument);
    };
  }
  if (instruction == "jgz")
  {
    return [first_argument, second_argument](processor& proc)
    {
      proc.jump(first_argument, second_argument);
    };
  }

  throw logic_error("Invalid instruction: " + instruction);
}

vector<instruction> parse(istream& input)
{
  string line;
  vector<instruction> instructions;
  while (!input.eof())
  {
    getline(input, line);
    if (line != "")
    {
      auto instruction = parse_line(line);
      instructions.push_back(instruction);
    }
  }

  return instructions;
}

int solve_first(istream& input){

  auto instructions = parse(input);
  soundprocessor processor{ instructions };

  processor.start();

	return processor.get_last_recovered();
}

int solve_second(istream& input){

  auto instructions = parse(input);
  queue<register_t> queue0;
  queue<register_t> queue1;
  duetprocessor processor0{ 0, queue0, queue1, instructions };
  duetprocessor processor1{ 1, queue1, queue0, instructions };

  processor0.start();
  processor1.start();
  while (processor0.is_changed() || processor1.is_changed())
  {
    processor0.notify();
    processor1.notify();
  }

  return processor1.get_number_of_sends();
}

