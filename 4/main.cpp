#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

int solve_first(istream&);
int solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("aa bb cc dd ee\naa bb cc dd aa\naa bb cc dd aaa\n")) << endl;  // 2
    cout << solve_second(stringstream("abcde fghij\nabcde xyz ecdab\na ab abc abd abf abj\niiii oiii ooii oooi oooo\noiii ioii iioi iiio\n")) << endl;  // 3

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

vector<string> parse_passphrase(string& line)
{
  auto line_parser = stringstream{ line };
  string token;
  vector<string> words;
  while (!line_parser.eof())
  {
    getline(line_parser, token, ' ');
    words.push_back(token);
  }

  return words;
}

bool is_passphrase_valid(vector<string>& words, function<bool(string, string)> validator)
{
  for (int i = 0; i < words.size() - 1; i++)
  {
    for (int j = i + 1; j < words.size(); j++)
    {
      if (validator(words[i], words[j]))
      {
        return false;
      }
    }
  }
  return true;
}

int count_valid_annagrams(istream& input, function<bool(string, string)> validator)
{
  int sum = 0;
  while (!input.eof())
  {
    string line;
    getline(input, line);
    if (line != "")
    {
      auto words = parse_passphrase(line);
      if (is_passphrase_valid(words, validator))
      {
        sum++;
      }
    }
  }
  return sum;
}

int solve_first(istream& input){

  return count_valid_annagrams(input, [](string a, string b) { return a == b; });
}

int solve_second(istream& input){

  return count_valid_annagrams(input, [](string a, string b) 
  {
    auto a_letters = vector<char>{ begin(a), end(a) };
    auto b_letters = vector<char>{ begin(b), end(b) };
    sort(begin(a_letters), end(a_letters));
    sort(begin(b_letters), end(b_letters));

    return a_letters.size() == b_letters.size() && equal(begin(a_letters), end(a_letters), begin(b_letters));
  });
}

