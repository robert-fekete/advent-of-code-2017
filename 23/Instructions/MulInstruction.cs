﻿using _23.Assembly;
using System;

namespace _23.Instructions
{
    internal class MulInstruction : BinaryInstruction
    {
        private readonly string registerName;

        public MulInstruction(string registerName, int constant) : base(registerName, constant)
        {
            this.registerName = registerName;
        }

        public MulInstruction(string registerName1, string registerName2) : base(registerName1, registerName2)
        {
            registerName = registerName1;
        }

        protected override void Executelongernal(Registry registry, ref int instructionPolonger, ref long solutionCounter)
        {
            var current = first.GetValue(registry);
            var value = second.GetValue(registry);
            registry.Set(registerName, current * value);

            solutionCounter++;
        }
    }
}
