#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>
#include <cassert>
#include <locale>
#include <cctype>
#include  <iomanip>

using namespace std;

int solve_first(istream&, int);
string solve_second(istream&);

int main(int argc, char* argv[])
{
    cout << solve_first(stringstream("3, 4, 1, 5"), 5) << endl;  // 12
    cout << solve_second(stringstream("")) << endl;  // a2582a3a0e66e6e86e3812dcb672a272
    cout << solve_second(stringstream("AoC 2017")) << endl;  // 33efeb34ea91902bb2f59c9920caa6cd
    cout << solve_second(stringstream("1,2,3")) << endl;  // 3efbe78a8d82f29979031a4aa0b16a9d
    cout << solve_second(stringstream("1,2,4 ")) << endl;  // 63960835bcdc130f0b66d7ff4f6a5a8e
    cout << solve_second(stringstream("225,171,131,2,35,5,0,13,1,246,54,97,255,98,254,110")) << endl; //

	auto f = ifstream("input.txt");
	if (f.is_open()){
		cout << solve_first(f, 256) << endl;
		f.clear();
		f.seekg(0, ios::beg);
		cout << solve_second(f) << endl;
	}
	else{
		cout << "File not found" << endl;
	}

	return 0;
}

char to_alphanum(int num)
{
  switch (num)
  {
  case 0:
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
  case 6:
  case 7:
  case 8:
  case 9:
    return num + '0';
  case 10:
    return 'a';
  case 11:
    return 'b';
  case 12:
    return 'c';
  case 13:
    return 'd';
  case 14:
    return 'e';
  case 15:
    return 'f';
  default:
    throw "Only numbers under 16 can be converted to hex char";
  }
}

string to_hex(int num)
{
  stringstream s;
  s << to_alphanum(num / 16);
  s << to_alphanum(num % 16);
  return s.str();
}

class HashGenerator
{
  vector<int> numbers;
  int skip = 0;
  int current_position = 0;

public:
  HashGenerator(int size)
    : numbers(vector<int>(size))
  {
    reset();
  }

  string get_dense_hash(string& input)
  {
    reset();
    vector<int> hash_input;
    for (auto c : input)
    {
      hash_input.push_back(c);
    }
    int suffix[]{ 17, 31, 73, 47, 23 };
    for (auto i : suffix)
    {
      hash_input.push_back(i);
    }
    return create_dense_hash(hash_input);
  }

  vector<int> get_sparse_hash(int rounds, vector<int>& input)
  {
    reset();
    create_sparse_hash(rounds, input);
    return numbers;
  }

  void print()
  {
    for (auto num : numbers)
    {
      cout << num << " ";
    }
    cout << endl;
  }

private:

  void twist(int position, int length)
  {
    for (int i = 0; i < length / 2; i++)
    {
      int left_index = (position + i) % numbers.size();
      int right_index = (position + length - i - 1) % numbers.size();
      swap(numbers[left_index], numbers[right_index]);
    }
  }

  void round(vector<int>& lengths)
  {
    for (auto length : lengths)
    {
      twist(current_position, length);
      current_position += length;
      current_position += skip;
      skip++;
      //print();
    }
  }

  void reset()
  {
    for (int i = 0; i < numbers.size(); i++)
    {
      numbers[i] = i;
    }
    skip = 0;
    current_position = 0;
  }

  void create_sparse_hash(int rounds, vector<int>& input)
  {
    for (int i = 0; i < rounds; i++)
    {
      round(input);
    }
  }

  string create_dense_hash(vector<int>& input)
  {
    assert(numbers.size() == 256);
    create_sparse_hash(64, input);

    stringstream result;
    for (int i = 0; i < 16; i++)
    {
      int accumulate = 0;
      for (int j = 0; j < 16; j++)
      {
        int index = i * 16 + j;
        accumulate ^= numbers[index];
      }
      result << to_hex(accumulate);
    }

    return result.str();
  }
};

vector<int> parse_lengths(istream& input)
{
  vector<int> lengths;
  string token;
  while (!input.eof())
  {
    getline(input, token, ',');
    if (token != "")
    {
      lengths.push_back(stoi(token));
    }
  }

  return lengths;
}

int solve_first(istream& input, int size){

  auto lengths = parse_lengths(input);
  auto generator = HashGenerator(size);

  auto numbers = generator.get_sparse_hash(1, lengths);

	return numbers[0] * numbers[1];
}

string solve_second(istream& input){

  string line;
  getline(input, line);

  auto generator = HashGenerator(256);
  auto result = generator.get_dense_hash(line);

	return result;
}

