﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _23.Assembly
{
    internal abstract class BinaryInstruction : IInstruction
    {
        protected readonly Argument first;
        protected readonly Argument second;

        public BinaryInstruction(int constant1, int constant2)
        {
            first = new Argument(constant1);
            second = new Argument(constant2);
        }

        public BinaryInstruction(string registerName, int constant)
        {
            first = new Argument(registerName);
            second = new Argument(constant);
        }

        public BinaryInstruction(string registerName1, string registerName2)
        {
            first = new Argument(registerName1);
            second = new Argument(registerName2);
        }

        public BinaryInstruction(int constant, string registerName)
        {
            first = new Argument(constant);
            second = new Argument(registerName);
        }

        public void Execute(Registry registry, ref int instructionPointer, ref long solutionCounter)
        {
            Executelongernal(registry, ref instructionPointer, ref solutionCounter);
        }

        protected abstract void Executelongernal(Registry registry, ref int instructionPolonger, ref long solutionCounter);
    }
}
