#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include <vector>
#include <functional>

using namespace std;

int solve_first(size_t);
int solve_second(size_t);

int main(int argc, char* argv[])
{
  cout << solve_first(3) << endl;  // 638
  cout << solve_first(329) << endl;
	cout << solve_second(329) << endl; 

	return 0;
}

void print(vector<int>& p)
{
  for (auto i : p)
  {
    cout << i << " ";
  }
  cout << endl;
}

struct insertion
{
  int position;
  int value;

  insertion(int position, int value)
    : position(position),
    value(value)
  {}
};

class insertioncalculator
{
  size_t steps;
  size_t size;
  size_t position;

public:
  insertioncalculator(size_t steps)
    : steps(steps),
    position(0),
    size(1)
  {}

  insertion get_next()
  {
    position = (position + steps) % size;
    position++;
    auto result = insertion(position, size);
    size++;

    return result;
  }
};

class spinlock
{
  size_t max_size;
  vector<int> numbers;
  insertioncalculator& calculator;

public:
  spinlock(size_t max_size, insertioncalculator& calculator)
    : numbers(vector<int>{ 0 }),
    max_size(max_size),
    calculator(calculator)
  {}

  void generate()
  {
    size_t size = 1;
    while (size < max_size)
    {
      auto insertion = calculator.get_next();
      numbers.insert(begin(numbers) + insertion.position, insertion.value);
      size++;
    }
  }

  int get_value_after(int target)
  {
    auto position = find(begin(numbers), end(numbers), target);
    auto index = position - begin(numbers);
    auto result_index = (index + 1) % numbers.size();
    return numbers[result_index];
  }

private:

  void print()
  {
    for (auto i : numbers)
    {
      cout << i << " ";
    }
    cout << endl;
  }
};

class stateless_spinlock
{
  insertioncalculator& calculator;
  size_t result;
  size_t max_size;

public:
  stateless_spinlock(size_t max_size, insertioncalculator& calculator)
    :calculator(calculator),
    max_size(max_size)
  { }

  void generate()
  {
    for(int i = 0; i < max_size; i++)
    {
      auto insertion = calculator.get_next();
      if (insertion.position == 1)
      {
        result = insertion.value;
      }
    }
  }

  int get_result()
  {
    return result;
  }
};

int solve_first(size_t steps){

  auto calculator = insertioncalculator{ steps };
  auto sp = spinlock{ 2018, calculator };
  sp.generate();

  return sp.get_value_after(2017);
}

int solve_second(size_t steps)
{
  auto calculator = insertioncalculator{ steps };
  auto sp = stateless_spinlock{ 50000000, calculator };
  sp.generate();

  return sp.get_result();
}

