﻿using System;
using System.IO;
using System.Linq;

namespace _25
{
    internal class Challenge25 : ChallengeBase<int, int, int>
    {
        public Challenge25()
        {
            var testInput = @"Begin in state A.
                              Perform a diagnostic checksum after 6 steps.

                              In state A:
                                If the current value is 0:
                                  - Write the value 1.
                                  - Move one slot to the right.
                                  - Continue with state B.
                                If the current value is 1:
                                  - Write the value 0.
                                  - Move one slot to the left.
                                  - Continue with state B.

                              In state B:
                                If the current value is 0:
                                  - Write the value 1.
                                  - Move one slot to the left.
                                  - Continue with state A.
                                If the current value is 1:
                                  - Write the value 1.
                                  - Move one slot to the right.
                                  - Continue with state A.";
            var input = File.ReadAllText(@".\input.txt");

            RegisterFirstTestCase(testInput, 0, 3);
            RegisterFirstRun(input, 0);
        }

        protected override int SolveFirst(string input, int param)
        {
            (var noSteps, var stateMachine) = ParseInput(input);

            var result = stateMachine.Run(noSteps);

            return result;
        }

        protected override int SolveSecond(string input, int param)
        {
            throw new NotImplementedException();
        }

        private (int, StateMachine) ParseInput(string input)
        {
            var lines = input.Split(Environment.NewLine).Select(l => l.Trim()).Where(l => !string.IsNullOrEmpty(l)).ToArray();

            var noSteps = int.Parse(lines[1].Split(' ')[5]);
            var stateMachine = StateMachineFactory.FromInput(lines);

            return (noSteps, stateMachine);
        }
    }
}
